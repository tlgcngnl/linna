<?php

namespace Linna_Framework\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use WP_Widget;

/**
 * Class Author
 *
 * @package Linna_Framework\Widgets
 */
class Author extends WP_Widget {

	/**
	 * @var string[]
	 */
	public static $defaults = array(
		'photo'    => '',
		'title'    => '',
		'subtitle' => '',
	);

	/**
	 * Author_Widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'Author',
			'description' => 'Show any photo with title and subtitle.',
		);
		parent::__construct( 'Author', 'Author Widget', $widget_ops );

		add_action( 'admin_enqueue_scripts', array( $this, 'widget_backend_scripts' ), 0 );
	}

	public function widget_backend_scripts() {
	}

	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance,
			self::$defaults,
		);

		?>
		<div class="pagebox">
			<p><?php _e( 'Title', 'linna-framework-by-mobius-studio' ); ?></p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</div>
		<div class="pagebox">
			<p><?php _e( 'Sub Title', 'linna-framework-by-mobius-studio' ); ?></p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" type="text" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" value="<?php echo esc_attr( $instance['subtitle'] ); ?>"/>
		</div>
		<div class="pagebox">
			<p><?php _e( 'Photo', 'linna-framework-by-mobius-studio' ); ?></p>

			<img src="<?php echo esc_url( wp_get_attachment_image_url( $instance['photo'] ) ); ?>" class="linna-widgets linna-widgets-image-picker-preview" id="preview-<?php echo esc_attr( $this->get_field_id( 'photo' ) ); ?>" />
			<button type="button" class="linna-widgets linna-widgets-image-picker button button-primary" data-of="<?php echo esc_attr( $this->get_field_id( 'photo' ) ); ?>">
				<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M324.3 64c3.3 0 6.3 2.1 7.5 5.2l22.1 58.8H464c8.8 0 16 7.2 16 16v288c0 8.8-7.2 16-16 16H48c-8.8 0-16-7.2-16-16V144c0-8.8 7.2-16 16-16h110.2l20.1-53.6c2.3-6.2 8.3-10.4 15-10.4h131m0-32h-131c-20 0-37.9 12.4-44.9 31.1L136 96H48c-26.5 0-48 21.5-48 48v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V144c0-26.5-21.5-48-48-48h-88l-14.3-38c-5.8-15.7-20.7-26-37.4-26zM256 408c-66.2 0-120-53.8-120-120s53.8-120 120-120 120 53.8 120 120-53.8 120-120 120zm0-208c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88z" class=""></path></svg>
				<span><?php esc_html_e( 'Select photo', 'linna-framework-by-mobius-studio' ); ?></span>
			</button>

			<button type="button" class="linna-widgets linna-widgets-image-unpicker button button-primary button-danger" data-of="<?php echo esc_attr( $this->get_field_id( 'photo' ) ); ?>">
				<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z" class=""></path></svg>
				<span><?php esc_html_e( 'Remove', 'linna-framework-by-mobius-studio' ); ?></span>
			</button>

			<input class="photo-id" id="<?php echo esc_attr( $this->get_field_id( 'photo' ) ); ?>" type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'photo' ) ); ?>" value="<?php echo esc_attr( $instance['photo'] ); ?>"/>
		</div>

		<div style="height: 30px"></div>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		foreach ( self::$defaults as $key => $arg ) {
			$instance[ $key ] = $new_instance[ $key ];
		}

		return $instance;
	}

	function widget( $args, $instance ) {
		echo "<div class='linna-author-widget'>";

		foreach ( array_keys( self::$defaults ) as $property ) {
			if ( empty( $instance[ $property ] ) ) {
				continue;
			}

			switch ( $property ) {
				case 'photo':
					echo wp_get_attachment_image( $instance[ $property ] );
					break;
				case 'title':
					echo sprintf( '<h4>%s</h4>', esc_html( $instance[ $property ] ) );
					break;
				case 'subtitle':
					echo sprintf( '<h5>%s</h5>', esc_html( $instance[ $property ] ) );
					break;
			}
		}

		echo '</div>';
	}
}
