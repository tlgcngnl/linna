<?php

namespace Linna_Framework\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Security {

	public function __construct() {

		if ( defined( 'MS_DEV' ) && MS_DEV ) {

			add_filter( 'https_ssl_verify', '__return_false' );

		}

	}

}
