<?php

namespace Linna_Framework\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Menu {
	/**
	 * EFramework_menu_handle->theme_name
	 * private
	 *
	 * @var string
	 */
	protected $theme_name = '';

	/**
	 * EFramework_menu_handle->theme_text_domain
	 * private
	 *
	 * @var string
	 */
	protected $theme_text_domain = '';

	public function __construct() {

		$current_theme           = wp_get_theme();
		$this->theme_name        = $current_theme->get( 'Name' );
		$this->theme_text_domain = $current_theme->get( 'TextDomain' );

		add_action( 'admin_menu', array( $this, 'linna_framework_add_menu' ) );

		add_action( 'admin_bar_menu', array( $this, 'linna_framework_add_admin_bar_menu' ), 100 );

	}

	public function linna_framework_add_menu() {

		add_menu_page(
			$this->theme_name,
			$this->theme_name,
			'manage_options',
			$this->theme_text_domain,
			array( $this, 'linna_framework_create_theme_dashboard' ),
			'dashicons-admin-generic',
			3
		);

		add_submenu_page(
			$this->theme_text_domain,
			$this->theme_name,
			esc_html__( 'Dashboard', 'linna-framework-by-mobius-studio' ),
			'manage_options',
			$this->theme_text_domain,
			array( $this, 'linna_framework_create_theme_dashboard' )
		);

		if ( class_exists( 'Linna_Demo_Importer' ) ) {

			add_submenu_page(
				$this->theme_text_domain,
				esc_html__( 'Linna Demo Importer', 'linna-framework-by-mobius-studio' ),
				esc_html__( 'Demo Importer', 'linna-framework-by-mobius-studio' ),
				'manage_options',
				admin_url( 'admin.php?page=linna-demo-importer' )
			);

		}

	}

	public function linna_framework_create_theme_dashboard() {
		new Dashboard();
	}

	function linna_framework_add_admin_bar_menu( $wp_admin_bar ) {

		$theme = wp_get_theme();

		/**
		 * Add "Theme Name" parent node
		 */
		$args = array(
			'id'    => $this->theme_text_domain,
			'title' => '<span class="ab-icon dashicons-smiley"></span>' . $theme->get( 'Name' ),
			'href'  => admin_url( 'admin.php?page=' . $this->theme_text_domain ),
			'meta'  => array(
				'class' => 'dashicons dashicons-admin-generic',
				'title' => $this->theme_text_domain,
			),
		);

		$wp_admin_bar->add_node( $args );

		/**
		 * Add Dashboard children node
		 */
		$args = array(
			'id'     => 'dashboard',
			'title'  => esc_html__( 'Dashboard', 'linna-framework-by-mobius-studio' ),
			'href'   => admin_url( 'admin.php?page=' . $this->theme_text_domain ),
			'parent' => $this->theme_text_domain,
			'meta'   => array(
				'class' => '',
				'title' => esc_html__( 'Dashboard', 'linna-framework-by-mobius-studio' ),
			),
		);

		$wp_admin_bar->add_node( $args );

		/**
		 * Add Import Export children node
		 */
		if ( class_exists( 'Linna_Demo_Importer' ) ) {

			$args = array(
				'id'     => 'linna-demo-importer',
				'title'  => esc_html__( 'Demo Importer', 'linna-framework-by-mobius-studio' ),
				'href'   => admin_url( 'admin.php?page=linna-demo-importer' ),
				'parent' => $this->theme_text_domain,
				'meta'   => array(
					'class' => '',
					'title' => esc_html__( 'Demo Importer', 'linna-framework-by-mobius-studio' ),
				),
			);

			$wp_admin_bar->add_node( $args );

		}

		/**
		 * Add Theme options children node
		 */
		if ( class_exists( 'ReduxFramework' ) && is_plugin_active( 'redux-framework/redux-framework.php' ) ) {

			$args = array(
				'id'     => 'theme-options',
				'title'  => esc_html__( 'Theme Options', 'linna-framework-by-mobius-studio' ),
				'href'   => admin_url( 'admin.php?page=theme-options' ),
				'parent' => $this->theme_text_domain,
				'meta'   => array(
					'class' => '',
					'title' => esc_html__( 'Theme Options', 'linna-framework-by-mobius-studio' ),
				),
			);

			$wp_admin_bar->add_node( $args );

		}

	}
}
