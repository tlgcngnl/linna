<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Loading_Screen {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Loading Screen', 'linna-framework-by-mobius-studio' ),
				'id'     => 'loading-screen',
				'icon'   => 'el el-screen-alt',
				'fields' => array(
					array(
						'id'      => 'loading-screen-status',
						'type'    => 'switch',
						'title'   => __( 'Status', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'          => 'transition-screen-status',
						'type'        => 'switch',
						'title'       => __( 'Page Transition', 'linna-framework-by-mobius-studio' ),
						'description' => __( 'Show loading screen as user clicks any in-site link.', 'linna-framework-by-mobius-studio' ),
						'default'     => true,
					),
					array(
						'id'          => 'loading-screen-show-always',
						'type'        => 'switch',
						'title'       => __( 'Show Always', 'linna-framework-by-mobius-studio' ),
						'description' => __( 'To Preview Changes. Can only be enabled while customizer is active. ', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'       => 'loading-screen-background',
						'type'     => 'color_gradient',
						'title'    => __( 'Background', 'linna-framework-by-mobius-studio' ),
						'output'   => array( '.linna-page-loading' ),
						'validate' => 'color',
						'default'  => array(
							'from' => '',
							'to'   => '',
						),
					),
					array(
						'id'      => 'loading-screen-type',
						'type'    => 'select',
						'title'   => __( 'Animation Type', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							'waves'  => __( 'Waves', 'linna-framework-by-mobius-studio' ),
							'one'    => __( 'One', 'linna-framework-by-mobius-studio' ),
							'two'    => __( 'Two', 'linna-framework-by-mobius-studio' ),
							'three'  => __( 'Three', 'linna-framework-by-mobius-studio' ),
							'four'   => __( 'Four', 'linna-framework-by-mobius-studio' ),
							'five'   => __( 'Five', 'linna-framework-by-mobius-studio' ),
							'six'    => __( 'Six', 'linna-framework-by-mobius-studio' ),
							'seven'  => __( 'Seven', 'linna-framework-by-mobius-studio' ),
							'eight'  => __( 'Eight', 'linna-framework-by-mobius-studio' ),
							'nine'   => __( 'Nine', 'linna-framework-by-mobius-studio' ),
							'ten'    => __( 'Ten', 'linna-framework-by-mobius-studio' ),
							'eleven' => __( 'Eleven', 'linna-framework-by-mobius-studio' ),
							'twelve' => __( 'Twelve', 'linna-framework-by-mobius-studio' ),
						),
					),
					array(
						'id'       => 'loading-screen-animation-color',
						'type'     => 'background',
						'title'    => __( 'Animation Color', 'linna-framework-by-mobius-studio' ),
						'output'   => array( '.linna-loading-1,.linna-loading-2 .double-bounce1,.linna-loading-2 .double-bounce2,.linna-loading-3 > div,.linna-loading-4 .cube1,.linna-loading-4 .cube2,.linna-loading-5,.linna-loading-6 .dot1,.linna-loading-6 .dot2,.linna-loading-7 > div,.linna-loading-8 .sk-child:before,.linna-loading-9 .sk-cube,.linna-loading-10 .sk-circle:before,.linna-loading-11 .sk-cube:before,.linna-loading-12 span,.linna-loading-waves-parallax' ),
						'validate' => 'color',
					),
					array(
						'id'    => 'loading-screen-text',
						'type'  => 'text',
						'title' => esc_html__( 'Text Message', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'     => 'loading-screen-text-color',
						'type'   => 'color_rgba',
						'title'  => __( 'Text Message Color', 'linna-framework-by-mobius-studio' ),
						'output' => array( '.linna-page-loading .linna-page-loading-text-message' ),
					),
				),
			),
		);
	}
}
