<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Logo {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Logo', 'linna-framework-by-mobius-studio' ),
				'id'     => 'logo',
				'desc'   => __( 'Logo options', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-bulb',
				'fields' => array(
					array(
						'id'       => 'header-logo-type',
						'type'     => 'select',
						'title'    => __( 'Logo Type', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'You can upload an image or use text as your logo', 'linna-framework-by-mobius-studio' ),
						'options'  => array(
							'image' => 'Image',
							'text'  => 'Text',
						),
					),
					array(
						'id'       => 'header-logo-text',
						'type'     => 'text',
						'title'    => esc_html__( 'Logo Text', 'linna-framework-by-mobius-studio' ),
						'subtitle' => esc_html__( 'Will use blog name If left empty.', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'          => 'header-logo-typography',
						'type'        => 'typography',
						'title'       => __( 'Logo Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'    => __( 'If you choose text as logo type', 'linna-framework-by-mobius-studio' ),
						'font-backup' => true,
						'all_styles'  => true,
						'output'      => array( '.site-logo-text' ),
						'units'       => 'px',
					),
					array(
						'id'       => 'header-logo',
						'type'     => 'media',
						'url'      => true,
						'title'    => __( 'Image', 'linna-framework-by-mobius-studio' ),
						'compiler' => 'true',
					),
					array(
						'id'             => 'header-logo-dimensions',
						'type'           => 'dimensions',
						'output'         => array( 'header .site-logo' ),
						'units'          => array( 'px' ),
						'units_extended' => 'false',
						'title'          => __( 'Image Dimensions', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'       => 'header-logo-position',
						'type'     => 'select',
						'title'    => __( 'Position', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Align the logo.', 'linna-framework-by-mobius-studio' ),
						'options'  => array(
							''                            => 'Left',
							'site-justify-content-end'    => 'Right',
							'site-justify-content-center' => 'Center',
						),
						'default'  => '',
					),
				),
			),
		);
	}
}
