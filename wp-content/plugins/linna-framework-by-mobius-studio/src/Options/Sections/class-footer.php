<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Footer {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Footer', 'linna-framework-by-mobius-studio' ),
				'id'     => 'footer',
				'desc'   => __( 'Footer options', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-download-alt',
				'fields' => array(
					array(
						'id'            => 'footer-padding',
						'type'          => 'spacing',
						'output'        => array( '.site-footer' ),
						'units'         => 'px',
						'mode'          => 'padding',
						'all'           => false,
						'display_units' => true,
						'title'         => __( 'Content Padding', 'linna-framework-by-mobius-studio' ),
						'default'       => array(
							'padding-top'    => '20px',
							'padding-right'  => '15px',
							'padding-bottom' => '20px',
							'padding-left'   => '15px',
						),
					),
					array(
						'id'      => 'footer-border',
						'type'    => 'border',
						'title'   => __( 'Footer Border', 'linna-framework-by-mobius-studio' ),
						'output'  => array( '.site-footer' ),
						'default' => array(
							'border-color'  => 'rgba(0,0,0,.12)',
							'border-style'  => 'solid',
							'border-top'    => '1px',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0',
						),
					),
					array(
						'id'       => 'footer-text',
						'type'     => 'text',
						'title'    => esc_html__( 'Footer&Copyright Text', 'linna-framework-by-mobius-studio' ),
						'subtitle' => esc_html__( 'Your copyright or any other text.', 'linna-framework-by-mobius-studio' ),
						'default'  => 'Copyright 2020 by Mobius Studio',
					),
					array(
						'id'          => 'footer-typography',
						'type'        => 'typography',
						'title'       => __( 'Footer Typography', 'linna-framework-by-mobius-studio' ),
						'font-backup' => true,
						'all_styles'  => true,
						'output'      => array( '.site-footer-text' ),
						'units'       => 'px',
						'default'     => array(
							'color'       => '#818181',
							'font-weight' => '400',
							'font-family' => 'Roboto',
							'font-backup' => '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
							'google'      => true,
							'font-size'   => '10px',
						),
					),

					array(
						'id'      => 'footer-socials',
						'type'    => 'social_icons_field',
						'title'   => esc_html__( 'Socials', 'linna-framework-by-mobius-studio' ),
						'hint'    => array(
							'content' => 'Create as many as you wish.',
						),
						'options' => array(),
						// For checkbox mode.
						'default' => array(),
					),
				),
			),
		);
	}
}
