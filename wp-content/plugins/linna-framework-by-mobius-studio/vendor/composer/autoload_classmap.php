<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Linna_Framework\\Admin\\Dashboard' => $baseDir . '/src/Admin/class-dashboard.php',
    'Linna_Framework\\Admin\\Menu' => $baseDir . '/src/Admin/class-menu.php',
    'Linna_Framework\\Admin\\Security' => $baseDir . '/src/Admin/class-security.php',
    'Linna_Framework\\Elementor\\Elementor' => $baseDir . '/src/Elementor/class-elementor.php',
    'Linna_Framework\\Elementor\\Elements' => $baseDir . '/src/Elementor/class-elements.php',
    'Linna_Framework\\Elementor\\Helpers' => $baseDir . '/src/Elementor/class-helpers.php',
    'Linna_Framework\\Elementor\\Widgets\\Posts_Carousel' => $baseDir . '/src/Elementor/Widgets/Posts_Carousel/class-posts-carousel.php',
    'Linna_Framework\\Elementor\\Widgets\\Product_Categories' => $baseDir . '/src/Elementor/Widgets/Product_Categories/class-product-categories.php',
    'Linna_Framework\\Elementor\\Widgets\\Timeline_Item' => $baseDir . '/src/Elementor/Widgets/Timeline_Item/class-timeline-item.php',
    'Linna_Framework\\Elementor\\Widgets\\Title_With_Subtitle' => $baseDir . '/src/Elementor/Widgets/Title_With_Subtitle/class-title-with-subtitle.php',
    'Linna_Framework\\Elementor\\Widgets\\Widget' => $baseDir . '/src/Elementor/Widgets/Widget_Interface.php',
    'Linna_Framework\\Icons\\Svg' => $baseDir . '/src/Icons/class-svg.php',
    'Linna_Framework\\Menu\\Front_End' => $baseDir . '/src/Menu/class-front-end.php',
    'Linna_Framework\\Menu\\Menu_Icons' => $baseDir . '/src/Menu/class-menu-icons.php',
    'Linna_Framework\\Menu\\Walker_Nav_Menu_Custom' => $baseDir . '/src/Menu/class-walker-nav-menu-custom.php',
    'Linna_Framework\\Options\\Loader' => $baseDir . '/src/Options/class-loader.php',
    'Linna_Framework\\Options\\Options' => $baseDir . '/src/Options/class-options.php',
    'Linna_Framework\\Options\\Sections\\Blog' => $baseDir . '/src/Options/Sections/class-blog.php',
    'Linna_Framework\\Options\\Sections\\Favicons' => $baseDir . '/src/Options/Sections/class-favicons.php',
    'Linna_Framework\\Options\\Sections\\Footer' => $baseDir . '/src/Options/Sections/class-footer.php',
    'Linna_Framework\\Options\\Sections\\General_Settings' => $baseDir . '/src/Options/Sections/class-general-settings.php',
    'Linna_Framework\\Options\\Sections\\Header' => $baseDir . '/src/Options/Sections/class-header.php',
    'Linna_Framework\\Options\\Sections\\Loading_Screen' => $baseDir . '/src/Options/Sections/class-loading-screen.php',
    'Linna_Framework\\Options\\Sections\\Logo' => $baseDir . '/src/Options/Sections/class-logo.php',
    'Linna_Framework\\Options\\Sections\\Menu' => $baseDir . '/src/Options/Sections/class-menu.php',
    'Linna_Framework\\Options\\Sections\\Page_Not_Found' => $baseDir . '/src/Options/Sections/class-page-not-found.php',
    'Linna_Framework\\Options\\Sections\\Sidebar' => $baseDir . '/src/Options/Sections/class-sidebar.php',
    'Linna_Framework\\Options\\Sections\\Typography' => $baseDir . '/src/Options/Sections/class-typography.php',
    'Linna_Framework\\Options\\Sections\\Webapp' => $baseDir . '/src/Options/Sections/class-webapp.php',
    'Linna_Framework\\WebApp\\WebApp' => $baseDir . '/src/WebApp/class-webapp.php',
    'Linna_Framework\\Widgets\\Author' => $baseDir . '/src/Widgets/class-author.php',
    'Linna_Framework\\Widgets\\Divider' => $baseDir . '/src/Widgets/class-divider.php',
    'Linna_Framework\\Widgets\\Widgets' => $baseDir . '/src/Widgets/class-widgets.php',
    'Linna_Framework\\Woocommerce\\Category_Icons\\Category_Icons' => $baseDir . '/src/Woocommerce/Category_Icons/class-category-icons.php',
    'Linna_Framework\\Woocommerce\\Woocommerce_Extend' => $baseDir . '/src/Woocommerce/class-woocommerce-extend.php',
);
