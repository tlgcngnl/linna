<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Menu {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Menu', 'linna-framework-by-mobius-studio' ),
				'id'     => 'primary-menu',
				'desc'   => __( 'Primary Menu', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-th-list',
				'fields' => array(
					array(
						'id'             => 'menu-typography',
						'type'           => 'typography',
						'title'          => __( 'Menu Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for main menu.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( '.main-navigation li a, .main-navigation .submenu-expand' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'          => 'menu-expanded-item-color',
						'type'        => 'typography',
						'title'       => __( 'Expanded Item Color', 'linna-framework-by-mobius-studio' ),
						'all_styles'  => false,
						'font-weight' => false,
						'font-style'  => false,
						'font-size'   => false,
						'font-family' => false,
						'line-height' => false,
						'text-align'  => false,
						'output'      => array( '.main-navigation li a[aria-expanded="true"], .main-navigation a[aria-expanded="true"] + .submenu-expand' ),
					),
					array(
						'id'             => 'menu-current-page-color',
						'type'           => 'typography',
						'title'          => __( 'Active Menu Color', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( '.main-navigation li.current-menu-item > a' ),
					),
					array(
						'id'            => 'menu-item-inner-spacing',
						'type'          => 'spacing',
						'title'         => __( 'Inner Spacing', 'linna-framework-by-mobius-studio' ),
						'output'        => array( '.main-navigation li a' ),
						'units'         => 'px',
						'mode'          => 'padding',
						'all'           => true,
						'display_units' => true,
					),
					array(
						'id'     => 'menu-item-icon-size',
						'type'   => 'dimensions',
						'title'  => __( 'Icon Size', 'linna-framework-by-mobius-studio' ),
						'output' => array( '.main-navigation .menu-icon-wrapper' ),
						'units'  => array( 'px' ),
						'width'  => true,
						'height' => true,
					),
					array(
						'id'     => 'menu-icon-color',
						'type'   => 'color_rgba',
						'title'  => __( 'Icon Color', 'linna-framework-by-mobius-studio' ),
						'google' => false,
						'output' => array( '.main-navigation .menu-icon-wrapper' ),
					),
					array(
						'id'     => 'current-menu-icon-color',
						'type'   => 'color_rgba',
						'title'  => __( 'Current Page Icon Color', 'linna-framework-by-mobius-studio' ),
						'google' => false,
						'output' => array( '.main-navigation li.current-menu-item > a .menu-icon-wrapper' ),
					),
					array(
						'id'     => 'menu-icon-bg-color',
						'type'   => 'color_rgba',
						'title'  => __( 'Icon Background Color', 'linna-framework-by-mobius-studio' ),
						'google' => false,
						'output' => array( '.main-navigation .menu-icon-wrapper' ),
					),
					array(
						'id'     => 'current-menu-icon-bg-color',
						'type'   => 'color_rgba',
						'title'  => __( 'Current Page Icon Color', 'linna-framework-by-mobius-studio' ),
						'google' => false,
						'output' => array( '.main-navigation li.current-menu-item > a .menu-icon-wrapper' ),
					),
				),
			),
		);
	}
}
