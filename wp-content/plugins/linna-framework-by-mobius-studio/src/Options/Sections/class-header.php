<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Header {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Header', 'linna-framework-by-mobius-studio' ),
				'id'     => 'header',
				'desc'   => __( 'Every option related to header.', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-website',
				'fields' => array(
					array(
						'id'             => 'header-height',
						'type'           => 'dimensions',
						'output'         => array( '.site-header .site-row' ),
						'units'          => array( 'px' ),
						'units_extended' => 'false',
						'title'          => __( 'Height', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Header Height (Logo cannot exceed the height of header)', 'linna-framework-by-mobius-studio' ),
						'width'          => false,
					),
					array(
						'id'       => 'header-background',
						'type'     => 'background',
						'output'   => array( '.site-header' ),
						'title'    => __( 'Header Background', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Header background with image, color, etc.', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'       => 'header-icon-color',
						'type'     => 'color_rgba',
						'title'    => __( 'Icon Colors', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Specify the body font properties.', 'linna-framework-by-mobius-studio' ),
						'google'   => false,
						'output'   => array( '.site-header a, .site-header button' ),
					),
					array(
						'id'       => 'header-sticky',
						'type'     => 'select',
						'title'    => __( 'Sticky/Fixed', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Keep header on screen all times or not.', 'linna-framework-by-mobius-studio' ),
						'options'  => array(
							''                     => 'Normal',
							'site-position-sticky' => 'Sticky / Fixed',
						),
					),
				),
			),
		);
	}
}
