import {Injectable} from '@angular/core';
import {ReplaySubject, Subject} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

declare var ajax_object: any;

@Injectable({
	providedIn: 'root'
})
export class TranslationsService {
	translations$:Subject<any[]> = new ReplaySubject<any[]>(1);

	get translate(){
		return this.translations$;
	}

	constructor(private http: HttpClient) {

		this.translations_wp_ajax().subscribe((translations) => {
			this.translations$.next(translations);
		});

	}

	translations_wp_ajax() {
		const params = new HttpParams({
			fromObject: {
				action: 'translations',
			}
		});

		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded',
			})
		};

		return this.http.post<any>(ajax_object.ajax_url, params, httpOptions);
	}
}
