import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

// import {AppRoutingModule} from './app-routing.module';
import {AppComponent,ImportConfirmationDialog} from './app.component';
import {APP_BASE_HREF} from "@angular/common";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatStepperModule} from "@angular/material/stepper";
import {ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {ErrorInterceptor} from "./error.interceptor";

@NgModule({
	declarations: [
		AppComponent,
		// ImportComponent,
		ImportConfirmationDialog,
	],
	imports: [
		BrowserModule,
		// AppRoutingModule,
		BrowserAnimationsModule,
		MatCardModule,
		MatButtonModule,
		MatToolbarModule,
		MatStepperModule,
		MatCheckboxModule,
		MatButtonToggleModule,
		MatIconModule,
		MatProgressSpinnerModule,
		MatDialogModule,
		ReactiveFormsModule,
		HttpClientModule,
	],
	providers: [
		// {provide: APP_BASE_HREF, useValue: 'admin.php?page=linna-demo-importer#/'},
		{provide: Window, useValue: window},
		{provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
	],
	bootstrap: [AppComponent]
})
export class AppModule {


}
