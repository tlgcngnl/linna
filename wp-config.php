<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'linna' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '}QC:}{9b+Nxh`9HmSk 7RyCcbex0!B<V9@~{)6d(B-P^(pAi<1[]HhoKn UImg<{' );
define( 'SECURE_AUTH_KEY', 'D8-$<>IZjiL%pr>vY.-9bG3@b;#_>g6ClNn&9A! `plO[5JrB2E?:]%lfDepO;#s' );
define( 'LOGGED_IN_KEY', 'Gs4Od5~d{<a:X|y=:C#%.AcJ?|/8t[-c^:5Rt!k6o/DxsjjQY319@[mp!E-gzSlk' );
define( 'NONCE_KEY', '#_$C*HX{$E#3OQ^mdv-Iki-#t%Wg%PrtcSJK|k=_{;sI6p4+;lvDo,gd:{H@W5`5' );
define( 'AUTH_SALT', '_b:XNn,Hi?L{WVfYKU$w02vaW3lH`i|Wf3{(ZKor_i| S*D]$v}d4Op~,,gi6pZE' );
define( 'SECURE_AUTH_SALT', '^ARh=`]7eiW:B/K(}C1MhK9Onpz&XmUOp7rhJhiJ=Kc|$m[?POyZ>S5I/>@6Z{@s' );
define( 'LOGGED_IN_SALT', 'OYX8wp)Bm [<m:tVEPa;J [q_<(.=jKb~Zh37Pgb_n&@d!k+W~~OPGB_Ir9/:YP/' );
define( 'NONCE_SALT', 'U5p3&]&#|n@k*/5l0mo@#:w7!v[$RbL-hDp:bOQyDG^^^v2nb|fmKHdi4ik<74[d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

define( 'MS_DEV', true );

define( 'WP_POST_REVISIONS', 0 );

//define('FORCE_SSL_ADMIN', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
