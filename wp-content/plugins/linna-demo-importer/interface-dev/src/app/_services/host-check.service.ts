import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {FormArray, FormGroup} from "@angular/forms";
import {DemoService} from "./demo.service";

declare var ajax_object: any;

@Injectable({
	providedIn: 'root'
})
export class HostCheckService {

	constructor(private http: HttpClient, private demoService: DemoService) {
	}

	check_wp_ajax(wp_function_name) {
		if( ! wp_function_name ){
			return;
		}

		const params = new HttpParams({
			fromObject: {
				action: 'host_tests',
				function_name: wp_function_name,
			}
		});

		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded',
			})
		};

		return this.http.post<any>(ajax_object.ajax_url, params, httpOptions);
	}

	checkFromFormFields(formGroup:FormGroup){
		const control_keys = Object.keys(formGroup.controls).filter(control_key => control_key.indexOf('host_check_') !== -1 );
		control_keys.forEach(control_key => {
			if ( control_key && control_key.indexOf('host_check_') !== -1 ){
				// check cache before making remote request
				if( control_key.indexOf('wp_remote_get') !== -1 ){
					let themeDemos = this.demoService.getWithExpiry('linna_theme_demo_cache');
					// console.log('themeDemos', themeDemos);
					if ( themeDemos ){
						this.demoService.demo$.next(themeDemos);

						formGroup['controls'][control_key].setValue(true);
						formGroup['controls'].loading['controls'][control_key].setValue(true);
						this.setSuccess(formGroup, control_keys);
						return;
					}
				}
				// console.log(control_key);
				this.check_wp_ajax(control_key).subscribe((result) => {
					// console.log(control_key , result);
					if( control_key.indexOf('wp_remote_get') !== -1 ){
						let body;
						try {
							body = JSON.parse(result.body)
						}catch (e) {
							body = false;
						}

						if( false !== body ){
							// console.log(body);
							this.demoService.demo$.next(body);
							this.demoService.setWithExpiry('linna_theme_demo_cache', body, 3600000);
							result = null;
						}else{
							// console.log(error);
							const errors = formGroup.controls.errors.value || {};
							errors[control_key] = 'An unexpected error occured. Please try again.';
							formGroup.controls.errors.setValue(errors);
							formGroup['controls'].loading['controls'][control_key].setValue(true);
						}
					}

					if ( result === null ){
						formGroup['controls'][control_key].setValue(true);
						formGroup['controls'].loading['controls'][control_key].setValue(true);
						this.setSuccess(formGroup, control_keys);
					}
				}, error => {
					// console.log(error);
					const errors = formGroup.controls.errors.value || {};
					errors[control_key] = error.error.message || 'An unexpected error occured. Please try again.';
					formGroup.controls.errors.setValue(errors);
					formGroup['controls'].loading['controls'][control_key].setValue(true);
				});
			}
		});
	}

	setSuccess(formGroup:FormGroup, control_keys:string[]){
		let i = 0;
		control_keys.map((control_key) => {
			if ( formGroup.controls[control_key].valid ) {
				i++;
			}
		});

		if ( i === control_keys.length ){
			formGroup.controls.success.setValue(true);
		}
	}
}
