<?php
/**
 * Custom search form.
 *
 * @package Linna 1.0.0
 */

?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'linna' ); ?></span>
		<input
			type="search"
			class="search-field"
			placeholder="<?php echo esc_attr_x( 'Search…', 'placeholder', 'linna' ); ?>"
			value="<?php echo get_search_query(); ?>" name="s"
			title="<?php echo esc_attr_x( 'Search for:', 'label', 'linna' ); ?>" />
	</label>
	<button
		type="submit"
		class="search-submit"
		value="<?php echo esc_attr_x( 'Search', 'submit button', 'linna' ); ?>">
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'linna' ); ?></span>
		<svg aria-hidden="true" width="18" height="18" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z" class=""></path></svg>
	</button>
</form>
