<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Blog {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'Blog', 'linna-framework-by-mobius-studio' ),
				'id'     => 'blog-settings',
				'icon'   => 'el el-pencil-alt',
				'fields' => array(
					array(
						'id'      => 'blog-list-preview-position',
						'type'    => 'button_set',
						'title'   => __( 'Preview Position', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							'left'  => 'Left',
							'top'   => 'Top',
							'right' => 'Right',
						),
						'default' => 'top',
					),
					array(
						'id'      => 'blog-list-columns',
						'type'    => 'button_set',
						'title'   => __( 'Blog List Columns', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							'12' => '1',
							'6'  => '2',
							'4'  => '3',
						),
						'default' => '12',
					),
					array(
						'id'      => 'blog-list-show-excerpt',
						'type'    => 'switch',
						'title'   => __( 'Excerpt', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'            => 'blog-list-excerpt-length',
						'type'          => 'slider',
						'title'         => __( 'Excerpt Character Length', 'linna-framework-by-mobius-studio' ),
						'default'       => 55,
						'min'           => 1,
						'step'          => 5,
						'max'           => 300,
						'display_value' => 'label',
					),
					array(
						'id'      => 'blog-list-sidebar',
						'type'    => 'switch',
						'title'   => __( 'Sidebar', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'      => 'blog-list-sidebar-position',
						'type'    => 'button_set',
						'title'   => __( 'Sidebar Position', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							'left'  => 'Left',
							'right' => 'Right',
						),
						'default' => 'right',
					),
					array(
						'id'      => 'blog-list-sidebar-size',
						'type'    => 'button_set',
						'title'   => __( 'Sidebar Size out of 12 Columns', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							'2' => '2/12',
							'3' => '3/12',
							'4' => '4/12',
							'5' => '5/12',
							'6' => '6/12',
						),
						'default' => '3',
					),
				),
			),
		);
	}
}
