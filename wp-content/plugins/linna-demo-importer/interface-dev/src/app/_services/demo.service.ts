import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';

import {Demo} from "../_classes/demo";
import {FormArray, FormGroup} from "@angular/forms";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {catchError, retry} from "rxjs/operators";

declare var ajax_object: any;

@Injectable({providedIn: 'root'})
export class DemoService {
    demo$ = new ReplaySubject<Demo[]>(1);
    demoLoading$ = new ReplaySubject<boolean>(1);

    constructor(private http: HttpClient) {}

	media_download_wp_ajax(url) {
		if( ! url ){
			return;
		}

		const params = new HttpParams({
			fromObject: {
				action: 'media_download',
				url: url,
			}
		});

		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded',
			})
		};

		return this.http.post<any>(ajax_object.ajax_url, params, httpOptions).toPromise();
	}

	sql_download_wp_ajax(zip, sql, options) {
		if( ! zip && ! sql && ! options ){
			return;
		}

		const params = new HttpParams({
			fromObject: {
				action: 'sql_download',
				zip: zip,
				imp: sql,
				options: options,
			}
		});

		const httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/x-www-form-urlencoded',
			})
		};

		return this.http.post<any>(ajax_object.ajax_url, params, httpOptions);
	}

	async processImport(formGroup:FormGroup){
		const mediaControls:FormGroup[] = (formGroup.controls.media as FormArray).controls as FormGroup[];
		for (const mediaControl of mediaControls) {
			// mediaControl.controls.complete.setValue(true);

			await this.media_download_wp_ajax(mediaControl.controls.url.value);

			// this.media_download_wp_ajax(mediaControl.controls.url.value).subscribe((result) => {
				// console.log(result);
				mediaControl.controls.complete.setValue(true);

				if ( mediaControls.filter(control => control.valid).length === mediaControls.length ){
					formGroup.controls.media_status.setValue(true);
				}
			/*}, (error) => {
				// console.log(result);
				mediaControl.controls.complete.setValue(true);

				if ( mediaControls.filter(control => control.valid).length === mediaControls.length ){
					formGroup.controls.media_status.setValue(true);
				}
			});*/
		}

		this.sql_download_wp_ajax(formGroup.controls.sql_zip.value, formGroup.controls.sql_imp.value, formGroup.controls.options.value).subscribe((result) => {
			// console.log(result);
			formGroup.controls.sql_status.setValue(true);
		}, (error) => {
		});


	}

	setWithExpiry(key, value, ttl) {
		const now = new Date();

		// `item` is an object which contains the original value
		// as well as the time when it's supposed to expire
		const item = {
			value: value,
			expiry: now.getTime() + ttl
		};
		localStorage.setItem(key, JSON.stringify(item))
	}

	getWithExpiry(key) {
		const itemStr = localStorage.getItem(key);
		// if the item doesn't exist, return null
		if (!itemStr) {
			return null
		}
		const item = JSON.parse(itemStr);
		const now = new Date();
		// compare the expiry time of the item with the current time
		if (now.getTime() > item.expiry) {
			// If the item is expired, delete the item from storage
			// and return null
			localStorage.removeItem(key);
			return null
		}
		return item.value
	}
}
