<?php
/**
 * Posts Carousel Widget.
 *
 * @package Linna Framework
 * @category Mobius Studio Elementor
 * @since 1.0.0
 */

namespace Linna_Framework\Elementor\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Plugin;
use Elementor\Core\Schemes;
use Elementor\Widget_Base;
use Linna_Framework\Elementor\Helpers;

/**
 * Elementor Posts Carousel widget.
 *
 * A custom widget, supports blog posts with carousel mode.
 *
 * @since 1.0.0
 */
class Posts_Carousel extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Posts Carousel widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_name() {
		return 'posts_carousel';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Posts Carousel widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_title() {
		return __( 'Posts Carousel', 'linna-framework-by-mobius-studio' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Posts Carousel widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_icon() {
		return 'eicon-slider-push';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Posts Carousel widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_categories() {
		return array( 'linna-framework' );
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @return array Widget keywords.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_keywords() {
		return array( 'posts', 'carousel', 'slider', 'blog' );
	}

	/**
	 * Register Posts Carousel widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_carousel_options',
			array(
				'label' => __( 'Carousel Options', 'linna-framework-by-mobius-studio' ),
			)
		);

		$slides_to_show = range( 1, 10 );
		$slides_to_show = array_combine( $slides_to_show, $slides_to_show );

		$categories = get_categories(
			array(
				'orderby'    => 'name',
				'order'      => 'ASC',
				'hide_empty' => false,
			)
		);

		array_walk(
			$categories,
			function ( &$val, &$key ) {
				$key = $val->term_id;
				$val = $val->name;
			}
		);

		$categories = array( 0 => __( 'All', 'linna-framework-by-mobius-studio' ) ) + $categories;

		$this->add_control(
			'posts_categories',
			array(
				'label'    => __( 'Categories', 'linna-framework-by-mobius-studio' ),
				'type'     => Controls_Manager::SELECT2,
				'multiple' => true,
				'options'  => $categories,
				'default'  => array( 0 ),
			)
		);

		$this->add_control(
			'limit',
			array(
				'label'   => __( 'Number of Posts', 'linna-framework-by-mobius-studio' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 25,
				'step'    => 1,
				'default' => 10,
			)
		);

		$this->add_control(
			'orderby',
			array(
				'label'              => __( 'Order By', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'options'            => array(
					'date'  => __( 'Date', 'linna-framework-by-mobius-studio' ),
					'title' => __( 'Title', 'linna-framework-by-mobius-studio' ),
					'rand'  => __( 'Rand', 'linna-framework-by-mobius-studio' ),
				),
				'default'            => 'date',
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'order',
			array(
				'label'              => __( 'Sort Order', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'options'            => array(
					'desc' => __( 'Descending', 'linna-framework-by-mobius-studio' ),
					'asc'  => __( 'Ascending', 'linna-framework-by-mobius-studio' ),
				),
				'default'            => 'desc',
				'frontend_available' => true,
			)
		);

		$this->add_responsive_control(
			'slides_to_show',
			array(
				'label'              => __( 'Slides to Show', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => 3,
				'options'            => array(
					'' => __( 'Default', 'linna-framework-by-mobius-studio' ),
				) + $slides_to_show,
				'frontend_available' => true,
			)
		);

		$this->add_responsive_control(
			'slides_to_scroll',
			array(
				'label'              => __( 'Slides to Scroll', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'description'        => __( 'Set how many slides are scrolled per swipe.', 'linna-framework-by-mobius-studio' ),
				'default'            => 1,
				'options'            => array(
					'' => __( 'Default', 'linna-framework-by-mobius-studio' ),
				) + $slides_to_show,
				'condition'          => array(
					'slides_to_show!' => '1',
				),
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'image_stretch',
			array(
				'label'   => __( 'Image Stretch', 'linna-framework-by-mobius-studio' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => array(
					'no'  => __( 'No', 'linna-framework-by-mobius-studio' ),
					'yes' => __( 'Yes', 'linna-framework-by-mobius-studio' ),
				),
			)
		);

		$this->add_control(
			'navigation',
			array(
				'label'              => __( 'Navigation', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => 'none',
				'options'            => array(
					'both'   => __( 'Arrows and Dots', 'linna-framework-by-mobius-studio' ),
					'arrows' => __( 'Arrows', 'linna-framework-by-mobius-studio' ),
					'dots'   => __( 'Dots', 'linna-framework-by-mobius-studio' ),
					'none'   => __( 'None', 'linna-framework-by-mobius-studio' ),
				),
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'view',
			array(
				'label'   => __( 'View', 'linna-framework-by-mobius-studio' ),
				'type'    => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			array(
				'label' => __( 'Additional Options', 'linna-framework-by-mobius-studio' ),
			)
		);

		$this->add_control(
			'pause_on_interaction',
			array(
				'label'              => __( 'Pause on Interaction', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => false,
				'options'            => array(
					true  => __( 'Yes', 'linna-framework-by-mobius-studio' ),
					false => __( 'No', 'linna-framework-by-mobius-studio' ),
				),
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'autoplay',
			array(
				'label'              => __( 'Autoplay', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => true,
				'options'            => array(
					true  => __( 'Yes', 'linna-framework-by-mobius-studio' ),
					false => __( 'No', 'linna-framework-by-mobius-studio' ),
				),
				'frontend_available' => true,
				'render_type'        => 'none',
				'separator'          => 'after',
			)
		);

		$this->add_control(
			'autoplay_speed',
			array(
				'label'              => __( 'Autoplay Speed', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::NUMBER,
				'default'            => 5000,
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'infinite',
			array(
				'label'              => __( 'Infinite Loop', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => true,
				'options'            => array(
					true  => __( 'Yes', 'linna-framework-by-mobius-studio' ),
					false => __( 'No', 'linna-framework-by-mobius-studio' ),
				),
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'effect',
			array(
				'label'              => __( 'Effect', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => 'slide',
				'options'            => array(
					'slide' => __( 'Slide', 'linna-framework-by-mobius-studio' ),
					'fade'  => __( 'Fade', 'linna-framework-by-mobius-studio' ),
				),
				'condition'          => array(
					'slides_to_show' => '1',
				),
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'speed',
			array(
				'label'              => __( 'Animation Speed', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::NUMBER,
				'default'            => 500,
				'frontend_available' => true,
			)
		);

		$this->add_control(
			'direction',
			array(
				'label'              => __( 'Direction', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SELECT,
				'default'            => 'ltr',
				'options'            => array(
					'ltr' => __( 'Left', 'linna-framework-by-mobius-studio' ),
					'rtl' => __( 'Right', 'linna-framework-by-mobius-studio' ),
				),
				'frontend_available' => true,
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			array(
				'label'     => __( 'Navigation', 'linna-framework-by-mobius-studio' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => array(
					'navigation' => array( 'arrows', 'dots', 'both' ),
				),
			)
		);

		$this->add_control(
			'heading_style_arrows',
			array(
				'label'     => __( 'Arrows', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => array(
					'navigation' => array( 'arrows', 'both' ),
				),
			)
		);

		$this->add_control(
			'arrows_position',
			array(
				'label'        => __( 'Position', 'linna-framework-by-mobius-studio' ),
				'type'         => Controls_Manager::SELECT,
				'default'      => 'inside',
				'options'      => array(
					'inside'  => __( 'Inside', 'linna-framework-by-mobius-studio' ),
					'outside' => __( 'Outside', 'linna-framework-by-mobius-studio' ),
				),
				'prefix_class' => 'elementor-arrows-position-',
				'condition'    => array(
					'navigation' => array( 'arrows', 'both' ),
				),
			)
		);

		$this->add_control(
			'arrows_size',
			array(
				'label'     => __( 'Size', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => array(
					'px' => array(
						'min' => 20,
						'max' => 60,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} .elementor-swiper-button.elementor-swiper-button-prev, {{WRAPPER}} .elementor-swiper-button.elementor-swiper-button-next' => 'font-size: {{SIZE}}{{UNIT}};',
				),
				'condition' => array(
					'navigation' => array( 'arrows', 'both' ),
				),
			)
		);

		$this->add_control(
			'arrows_color',
			array(
				'label'     => __( 'Color', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => array(
					'{{WRAPPER}} .elementor-swiper-button.elementor-swiper-button-prev, {{WRAPPER}} .elementor-swiper-button.elementor-swiper-button-next' => 'color: {{VALUE}};',
				),
				'condition' => array(
					'navigation' => array( 'arrows', 'both' ),
				),
			)
		);

		$this->add_control(
			'heading_style_dots',
			array(
				'label'     => __( 'Dots', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => array(
					'navigation' => array( 'dots', 'both' ),
				),
			)
		);

		$this->add_control(
			'dots_position',
			array(
				'label'        => __( 'Position', 'linna-framework-by-mobius-studio' ),
				'type'         => Controls_Manager::SELECT,
				'default'      => 'outside',
				'options'      => array(
					'outside' => __( 'Outside', 'linna-framework-by-mobius-studio' ),
					'inside'  => __( 'Inside', 'linna-framework-by-mobius-studio' ),
				),
				'prefix_class' => 'elementor-pagination-position-',
				'condition'    => array(
					'navigation' => array( 'dots', 'both' ),
				),
			)
		);

		$this->add_control(
			'dots_size',
			array(
				'label'     => __( 'Size', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => array(
					'px' => array(
						'min' => 5,
						'max' => 10,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				),
				'condition' => array(
					'navigation' => array( 'dots', 'both' ),
				),
			)
		);

		$this->add_control(
			'dots_color',
			array(
				'label'     => __( 'Color', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => array(
					'{{WRAPPER}} .swiper-pagination-bullet' => 'background: {{VALUE}};',
				),
				'condition' => array(
					'navigation' => array( 'dots', 'both' ),
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_image',
			array(
				'label' => __( 'Slide Item', 'linna-framework-by-mobius-studio' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			)
		);

		$this->add_responsive_control(
			'gallery_vertical_align',
			array(
				'label'       => __( 'Vertical Align', 'linna-framework-by-mobius-studio' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => array(
					'flex-start' => array(
						'title' => __( 'Start', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-v-align-top',
					),
					'center'     => array(
						'title' => __( 'Center', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-v-align-middle',
					),
					'flex-end'   => array(
						'title' => __( 'End', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-v-align-bottom',
					),
				),
				'condition'   => array(
					'slides_to_show!' => '1',
				),
				'selectors'   => array(
					'{{WRAPPER}} .swiper-wrapper' => 'display: flex; align-items: {{VALUE}};',
				),
			)
		);

		$this->add_control(
			'image_spacing',
			array(
				'label'     => __( 'Spacing', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => array(
					''       => __( 'Default', 'linna-framework-by-mobius-studio' ),
					'custom' => __( 'Custom', 'linna-framework-by-mobius-studio' ),
				),
				'default'   => 'custom',
				'condition' => array(
					'slides_to_show!' => '1',
				),
			)
		);

		$this->add_responsive_control(
			'image_spacing_custom',
			array(
				'label'              => __( 'Spacing in PX', 'linna-framework-by-mobius-studio' ),
				'type'               => Controls_Manager::SLIDER,
				'range'              => array(
					'px' => array(
						'max' => 100,
					),
				),
				'default'            => array(
					'size' => 30,
				),
				'tablet_default'     => array(
					'size' => 15,
				),
				'mobile_default'     => array(
					'size' => 8,
				),
				'show_label'         => false,
				'condition'          => array(
					'image_spacing'   => 'custom',
					'slides_to_show!' => '1',
				),
				'frontend_available' => true,
				'render_type'        => 'none',
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'     => 'image_border',
				'selector' => '{{WRAPPER}} .linna-framework-posts-carousel-wrapper .linna-framework-posts-carousel .swiper-slide',
			)
		);

		$this->add_control(
			'image_border_radius',
			array(
				'label'      => __( 'Border Radius', 'linna-framework-by-mobius-studio' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'default'    => array(
					'unit'   => 'px',
					'top'    => '3',
					'right'  => '3',
					'bottom' => '3',
					'left'   => '3',
				),
				'selectors'  => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-wrapper .linna-framework-posts-carousel .swiper-slide' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			array(
				'name'      => 'slide_item',
				'selectors' => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-wrapper .linna-framework-posts-carousel .swiper-slide' => 'box-shadow: {{HORIZONTAL}}px {{VERTICAL}}px {{BLUR}}px {{SPREAD}} {{COLOR}};',
				),
				'default'   => array(
					'horizontal' => 0,
					'vertical'   => 0,
					'blur'       => '3',
					'spread'     => 0,
					'color'      => 'rgba(0,0,0,0.4)',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_caption',
			array(
				'label' => __( 'Caption', 'linna-framework-by-mobius-studio' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			)
		);

		$this->add_control(
			'caption_align',
			array(
				'label'     => __( 'Alignment', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => array(
					'left'    => array(
						'title' => __( 'Left', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-left',
					),
					'center'  => array(
						'title' => __( 'Center', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-center',
					),
					'right'   => array(
						'title' => __( 'Right', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-right',
					),
					'justify' => array(
						'title' => __( 'Justified', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-justify',
					),
				),
				'default'   => 'left',
				'selectors' => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-caption' => 'text-align: {{VALUE}};',
				),
			)
		);

		$this->add_control(
			'caption_text_color',
			array(
				'label'     => __( 'Text Color', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-caption' => 'color: {{VALUE}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'caption_typography',
				'scheme'   => Schemes\Typography::TYPOGRAPHY_3,
				'selector' => '{{WRAPPER}} .linna-framework-posts-carousel-caption',
			)
		);

		$this->add_responsive_control(
			'padding',
			array(
				'label'      => __( 'Padding', 'linna-framework-by-mobius-studio' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', 'em', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-caption' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_read_more',
			array(
				'label' => __( 'Read More Link', 'linna-framework-by-mobius-studio' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			)
		);

		$this->add_control(
			'read_more_align',
			array(
				'label'     => __( 'Alignment', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => array(
					'left'    => array(
						'title' => __( 'Left', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-left',
					),
					'center'  => array(
						'title' => __( 'Center', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-center',
					),
					'right'   => array(
						'title' => __( 'Right', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-right',
					),
					'justify' => array(
						'title' => __( 'Justified', 'linna-framework-by-mobius-studio' ),
						'icon'  => 'eicon-text-align-justify',
					),
				),
				'default'   => 'left',
				'selectors' => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-read-more-link' => 'text-align: {{VALUE}};',
				),
			)
		);

		$this->add_control(
			'read_more_text_color',
			array(
				'label'     => __( 'Text Color', 'linna-framework-by-mobius-studio' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => array(
					'{{WRAPPER}} .linna-framework-posts-carousel-read-more-link' => 'color: {{VALUE}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'read_more_typography',
				'scheme'   => Schemes\Typography::TYPOGRAPHY_3,
				'selector' => '{{WRAPPER}} .linna-framework-posts-carousel-read-more-link',
			)
		);

		$this->end_controls_section();
	}

	/**
	 * Render Posts Carousel widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$posts = get_posts(
			array(
				'numberposts'      => $settings['limit'],
				'category'         => implode( ',', $settings['posts_categories'] ),
				'orderby'          => $settings['orderby'],
				'order'            => $settings['order'],
				'post_type'        => 'post',
				'suppress_filters' => true,
			)
		);

		if ( empty( $posts ) ) {
			return;
		}

		$slides = array();

		global $post;
		foreach ( $posts as $post ) {
			setup_postdata( $post );

			$thumbnail = get_the_post_thumbnail();

			if ( function_exists( 'fly_get_attachment_image' ) ) {
				$thumbnail = fly_get_attachment_image( get_post_thumbnail_id(), 'blog_carousel' );
			}

			$read_more = sprintf( '<a href="%s" class="linna-framework-posts-carousel-read-more-link">%s</a>', get_the_permalink(), __( 'Read More...', 'linna-framework-by-mobius-studio' ) );

			$slide_html = '<div class="swiper-slide">';

			$slide_html .= sprintf( '<a href="%s">%s</a>', get_the_permalink(), $thumbnail );

			$slide_html .= sprintf( '<div class="linna-framework-posts-carousel-caption">%s%s</div>', get_the_title(), $read_more );

			$slide_html .= '</div>';

			$slides[] = $slide_html;

		}
		wp_reset_postdata();

		if ( empty( $slides ) ) {
			return;
		}

		$this->add_render_attribute(
			array(
				'carousel'         => array(
					'class' => 'linna-framework-posts-carousel swiper-wrapper',
				),
				'carousel-wrapper' => array(
					'class' => 'linna-framework-posts-carousel-wrapper swiper-container',
					'dir'   => $settings['direction'],
				),
			)
		);

		$show_dots   = ( in_array( $settings['navigation'], array( 'dots', 'both' ), true ) );
		$show_arrows = ( in_array( $settings['navigation'], array( 'arrows', 'both' ), true ) );

		if ( 'yes' === $settings['image_stretch'] ) {
			$this->add_render_attribute( 'carousel', 'class', 'swiper-image-stretch' );
		}

		$data_options = Helpers::map_elementor_options_to_swiper_options( $settings );

		$this->add_render_attribute( 'carousel-wrapper', 'data-options', wp_json_encode( $data_options ) );

		$slides_count = count( $posts );

		?>
		<div <?php echo $this->get_render_attribute_string( 'carousel-wrapper' ); ?>>
			<div <?php echo $this->get_render_attribute_string( 'carousel' ); ?>>
				<?php echo implode( '', $slides ); ?>
			</div>
			<?php if ( 1 < $slides_count ) : ?>
				<?php if ( $show_dots ) : ?>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
				<?php if ( $show_arrows ) : ?>
					<div class="elementor-swiper-button elementor-swiper-button-prev">
						<i class="eicon-chevron-left" aria-hidden="true"></i>
						<span class="elementor-screen-only"><?php _e( 'Previous', 'linna-framework-by-mobius-studio' ); ?></span>
					</div>
					<div class="elementor-swiper-button elementor-swiper-button-next">
						<i class="eicon-chevron-right" aria-hidden="true"></i>
						<span class="elementor-screen-only"><?php _e( 'Next', 'linna-framework-by-mobius-studio' ); ?></span>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php
	}

}
