/**
 * This file contains all the custom elementor widgets javascript files,
 * Loads them and initialize them for elementor edit mode.
 * Requires WebPack and Babel to compile for browser code.
 *
 * @package Linna Framework
 * @since 1.0.0
 */

import PostsCarouselAdmin from "./../../Widgets/Posts_Carousel/posts-carousel-admin";

PostsCarouselAdmin();

