<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Page_Not_Found {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'Page Not Found', 'linna-framework-by-mobius-studio' ),
				'id'     => 'page-not-found',
				'icon'   => 'el el-error',
				'fields' => array(
					array(
						'id'      => 'page-not-found-first-line-status',
						'type'    => 'switch',
						'title'   => __( 'First Line', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'          => 'page-not-found-first-line',
						'type'        => 'typography',
						'title'       => __( 'First Line Typography', 'linna-framework-by-mobius-studio' ),
						'font-backup' => true,
						'all_styles'  => true,
						'output'      => array( '.site-404 h1' ),
						'units'       => 'px',
					),
					array(
						'id'            => 'page-not-found-first-line-spacing',
						'type'          => 'spacing',
						'output'        => array( '.site-404 h1' ),
						'units'         => 'px',
						'mode'          => 'margin',
						'all'           => false,
						'display_units' => true,
						'title'         => __( 'First Line Margin', 'linna-framework-by-mobius-studio' ),
					),

					array(
						'id'      => 'page-not-found-second-line-status',
						'type'    => 'switch',
						'title'   => __( 'Second Line', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'          => 'page-not-found-second-line',
						'type'        => 'typography',
						'title'       => __( 'Second Line Typography', 'linna-framework-by-mobius-studio' ),
						'font-backup' => true,
						'all_styles'  => true,
						'output'      => array( '.site-404 span' ),
						'units'       => 'px',
					),
					array(
						'id'            => 'page-not-found-second-line-spacing',
						'type'          => 'spacing',
						'output'        => array( '.site-404 span' ),
						'units'         => 'px',
						'mode'          => 'margin',
						'all'           => false,
						'display_units' => true,
						'title'         => __( 'Second Line Margin', 'linna-framework-by-mobius-studio' ),
					),

					array(
						'id'      => 'page-not-found-third-line-status',
						'type'    => 'switch',
						'title'   => __( 'Third Line', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
					array(
						'id'          => 'page-not-found-third-line',
						'type'        => 'typography',
						'title'       => __( 'Third Line Typography', 'linna-framework-by-mobius-studio' ),
						'font-backup' => true,
						'all_styles'  => true,
						'output'      => array( '.site-404 article' ),
						'units'       => 'px',
					),
					array(
						'id'            => 'page-not-found-third-line-spacing',
						'type'          => 'spacing',
						'output'        => array( '.site-404 article' ),
						'units'         => 'px',
						'mode'          => 'margin',
						'all'           => false,
						'display_units' => true,
						'title'         => __( 'Third Line Margin', 'linna-framework-by-mobius-studio' ),
					),

					array(
						'id'      => 'page-not-found-search',
						'type'    => 'switch',
						'title'   => __( 'Search', 'linna-framework-by-mobius-studio' ),
						'default' => true,
					),
				),
			),
		);
	}
}
