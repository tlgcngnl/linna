<?php

namespace Linna_Framework\Elementor;

use Elementor\Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'LINNA_FRAMEWORK_ELEMENTOR__FILE__', __FILE__ );
define( 'LINNA_FRAMEWORK_ELEMENTOR_PATH', plugin_dir_path( LINNA_FRAMEWORK_ELEMENTOR__FILE__ ) );
define( 'LINNA_FRAMEWORK_ELEMENTOR_URL', plugins_url( '/', LINNA_FRAMEWORK_ELEMENTOR__FILE__ ) );
define( 'LINNA_FRAMEWORK_ELEMENTOR_ASSETS_URL', LINNA_FRAMEWORK_ELEMENTOR_URL . 'assets/' );

class Elementor {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '5.4';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Elementor The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', array( $this, 'i18n' ) );
		add_action( 'plugins_loaded', array( $this, 'init' ) );

	}

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Elementor An instance of the class.
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'linna-framework-by-mobius-studio' );

	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated.
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_missing_main_plugin' ) );

			return;
		}

		// Check for required Elementor version.
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_minimum_elementor_version' ) );

			return;
		}

		// Check for required PHP version.
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_minimum_php_version' ) );

			return;
		}

		// Add Plugin actions.
		add_action( 'elementor/elements/categories_registered', array( $this, 'add_elementor_widget_categories' ) );
		add_action( 'elementor/widgets/widgets_registered', array( $this, 'init_widgets' ) );
		add_action( 'elementor/controls/controls_registered', array( $this, 'init_controls' ) );
		add_action( 'elementor/frontend/after_enqueue_styles', array( $this, 'widget_styles' ) );
		add_action( 'elementor/preview/enqueue_styles', array( $this, 'editor_styles' ) );

	}

	/**
	 * Register and enqueue styles and scripts.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function widget_styles() {

		wp_register_style( 'linna-framework-elementor', LINNA_FRAMEWORK_ELEMENTOR_ASSETS_URL . 'dist/css/bundle.css', array(), self::VERSION );
		wp_register_script( 'linna-framework-elementor', LINNA_FRAMEWORK_ELEMENTOR_ASSETS_URL . 'dist/js/bundle.js', array(), self::VERSION, true );

		wp_enqueue_style( 'linna-framework-elementor' );

		wp_enqueue_script( 'swiper' );

		wp_enqueue_script( 'linna-framework-elementor' );

	}

	/**
	 * Register and enqueue styles and scripts for Elementor edit mode.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function editor_styles() {

		wp_register_script( 'linna-framework-elementor-admin', LINNA_FRAMEWORK_ELEMENTOR_ASSETS_URL . 'dist/js/bundle-admin.js', array(), self::VERSION, true );

		wp_enqueue_script( 'linna-framework-elementor-admin' );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( ( wp_verify_nonce( 'elementor' ) || wp_verify_nonce( 'linna-framework-elementor' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'linna-framework-by-mobius-studio' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( ( wp_verify_nonce( 'elementor' ) || wp_verify_nonce( 'linna-framework-elementor' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'linna-framework-by-mobius-studio' ) . '</strong>',
			self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( ( wp_verify_nonce( 'elementor' ) || wp_verify_nonce( 'linna-framework-elementor' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'linna-framework-by-mobius-studio' ) . '</strong>',
			self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		foreach ( Elements::$list as $file_name => $class_name ) {
			// Register widget.
			Plugin::instance()->widgets_manager->register_widget_type( new $class_name() );
		}

	}

	/**
	 * Add Elementor Widget Categories
	 *
	 * Create custom widget category for Mobius Studio
	 *
	 * @param object $elements_manager Elementors category manager.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function add_elementor_widget_categories( $elements_manager ) {

		$category_prefix = 'linna-';

		$elements_manager->add_category(
			'linna-framework',
			array(
				'title' => __( 'Linna Framework Builder', 'linna-framework-by-mobius-studio' ),
				'icon'  => 'fa fa-dna',
			)
		);

		$reorder_cats = function() use ( $category_prefix ) {
			uksort(
				$this->categories,
				function( $key_one, $key_two ) use ( $category_prefix ) {
					if ( substr( $key_one, 0, 6 ) == $category_prefix ) {
						return -1;
					}
					if ( substr( $key_two, 0, 6 ) == $category_prefix ) {
						return 1;
					}
					return 0;
				}
			);

		};
		$reorder_cats->call( $elements_manager );

	}

	/**
	 * Init Controls
	 *
	 * Include controls files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_controls() {}

}
