<?php
// TODO: comments

namespace Linna_Framework\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elements {

	static $list = array(
		'title-with-subtitle-widget' => '\Linna_Framework\Elementor\Widgets\Title_With_Subtitle',
		'posts-carousel'             => '\Linna_Framework\Elementor\Widgets\Posts_Carousel',
		'timeline-item'              => '\Linna_Framework\Elementor\Widgets\Timeline_Item',
		'product-categories'         => '\Linna_Framework\Elementor\Widgets\Product_Categories',
	);

}
