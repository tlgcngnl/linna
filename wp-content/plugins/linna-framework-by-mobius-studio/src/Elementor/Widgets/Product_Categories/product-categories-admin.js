/**
 * Swiper init for PostsCarousel Widget.
 *
 * @constructor
 * @package Linna Framework
 */

/* global jQuery */
import ProductCategories from "./product-categories";

export default function ProductCategoriesAdmin() {
	jQuery(
		function( $ ) {
			if ( window.elementorFrontend ) {
				elementorFrontend.hooks.addAction(
					'frontend/element_ready/product_categories.default',
					function( $scope ) {
						ProductCategories();
					}
				);
			}
		}
	);
}
