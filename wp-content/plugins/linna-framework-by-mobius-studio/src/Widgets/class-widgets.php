<?php

namespace Linna_Framework\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'LINNA_FRAMEWORK_WIDGETS__FILE__', __FILE__ );
define( 'LINNA_FRAMEWORK_WIDGETS_PATH', plugin_dir_path( LINNA_FRAMEWORK_WIDGETS__FILE__ ) );
define( 'LINNA_FRAMEWORK_WIDGETS_URL', plugins_url( '/', LINNA_FRAMEWORK_WIDGETS__FILE__ ) );
define( 'LINNA_FRAMEWORK_WIDGETS_ASSETS_URL', LINNA_FRAMEWORK_WIDGETS_URL . 'assets/' );

/**
 * Class Linna_Widgets
 */
class Widgets {
	public static $widgets = array(
		'class-divider' => 'Linna_Framework\Widgets\Divider',
		'class-author'  => 'Linna_Framework\Widgets\Author',
	);

	public function __construct() {
		// Enqueue custom required css and javascripts.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10, 1 );

		self::init_widgets();
	}

	/**
	 * Make sure to load script files only on plugins page.
	 * MS_DEV can only be defined by plugin owner to work on uncompiled code.
	 * Development and production files differs.
	 *
	 * Localize script handles to make ajax_object available.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function enqueue_scripts() {
		global $pagenow;

		if ( in_array( $pagenow, array( 'widgets.php', 'customize.php' ) ) ) {
			wp_enqueue_style( 'nouislider', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'libs/nouislider/nouislider.min.css', array(), '14.1.1' );
			wp_enqueue_script( 'nouislider', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'libs/nouislider/nouislider.min.js', null, '14.1.1', true );

			wp_enqueue_style( 'linna-widgets-style', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'styles.css', array( 'nouislider' ), LINNA_FRAMEWORK_VERSION );
			wp_enqueue_script( 'linna-widgets-custom-js', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'custom.js', array( 'jquery' ), LINNA_FRAMEWORK_VERSION, true );
		}

	}

	public function init_widgets() {
		foreach ( self::$widgets as $slug => $class_name ) {
			$widget_file_path = sprintf( '%s/%s.php', LINNA_FRAMEWORK_WIDGETS_PATH, $slug );
			if ( file_exists( $widget_file_path ) ) {
				if ( class_exists( $class_name ) ) {
					add_action(
						'widgets_init',
						function () use ( $class_name ) {
							register_widget( $class_name );
						}
					);
				}
			}
		}
	}
}
