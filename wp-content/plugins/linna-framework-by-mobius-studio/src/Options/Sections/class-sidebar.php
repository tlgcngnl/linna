<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Sidebar {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'Sidebar', 'linna-framework-by-mobius-studio' ),
				'id'     => 'sidebar-settings',
				'icon'   => 'el el-braille',
				'fields' => array(
					array(
						'id'      => 'sidebar-position',
						'type'    => 'button_set',
						'title'   => __( 'Sidebar Position', 'linna-framework-by-mobius-studio' ),
						'options' => array(
							esc_attr( 'side="left"' )  => 'Left',
							esc_attr( 'side="right"' ) => 'Right',
							'disabled'                 => 'Disabled',
						),
						'default' => 'side="left"',
					),
					array(
						'id'       => 'sidebar-background',
						'type'     => 'background',
						'output'   => array( '.site-sidebar' ),
						'title'    => __( 'Sidebar Background', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Sidebar background with image, color, etc.', 'linna-framework-by-mobius-studio' ),
						'default'  => array(),
					),
					array(
						'id'      => 'sidebar-text-color',
						'type'    => 'color_rgba',
						'title'   => __( 'Text Color', 'linna-framework-by-mobius-studio' ),
						'google'  => false,
						'output'  => array( '.site-sidebar, .site-sidebar h1, .site-sidebar h2, .site-sidebar h3, .site-sidebar h4, .site-sidebar h5, .site-sidebar h6' ),
						'default' => array(),
					),
					array(
						'id'      => 'sidebar-link-color',
						'type'    => 'link_color',
						'title'   => __( 'Links Color', 'linna-framework-by-mobius-studio' ),
						'output'  => array( '.site-sidebar a' ),
						'default' => array(),
					),
					array(
						'id'            => 'sidebar-box-padding',
						'type'          => 'spacing',
						'output'        => array( '.site-sidebar .sidebar-box' ),
						'units'         => 'px',
						'mode'          => 'padding',
						'all'           => false,
						'display_units' => true,
						'title'         => __( 'Content Padding', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'      => 'sidebar-top-caret-color',
						'type'    => 'color_rgba',
						'title'   => __( 'Sidebar Top Caret Color (Closing Button)', 'linna-framework-by-mobius-studio' ),
						'google'  => false,
						'output'  => array( '.site-sidebar .site-sidebar-toggle' ),
						'default' => array(),
					),
				),
			),
		);
	}
}
