<?php
/**
 * Put favicons and web app related tags between head,
 * Create manifest file for browsers with web app information in it.
 * Update manifest file when an option
 *
 * @package WordPress
 * @subpackage Linna
 * @since 1.0.0
 */

namespace Linna_Framework\WebApp;

use Linna_Framework\Options\Options;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * This class is in charge of displaying SVG icons across the site.
 *
 * Place each <svg> source on its own array key, without adding the
 * both `width` and `height` attributes, since these are added dnamically,
 * before rendering the SVG code.
 *
 * All icons are assumed to have equal width and height, hence the option
 * to only specify a `$size` parameter in the svg methods.
 *
 * @since 1.0.0
 */
class WebApp {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Redux (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Redux Framework installed and activated.
		if ( ! class_exists( 'Redux' ) || ! is_plugin_active( 'redux-framework/redux-framework.php' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_missing_main_plugin' ) );

			return;
		}

		add_action( 'wp_head', array( $this, 'apple_related' ), 0 );
		add_action( 'wp_head', array( $this, 'manifest_related' ), 0 );
		add_action( 'wp_head', array( $this, 'others' ), 0 );
		add_action( 'wp_footer', array( $this, 'register_sw' ), 9 );
		add_action( 'redux/options/linna_theme_options/settings/change', array( $this, 'manifest_related' ), 0 );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Redux Framework installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( ( wp_verify_nonce( 'redux' ) || wp_verify_nonce( 'linna-framework-webapp' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: Redux Framework */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'Redux Framework', 'linna-framework-by-mobius-studio' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}

	public function apple_related() {
		if ( function_exists( 'Options::get' ) ) {

			foreach ( Options::apple_touch_icons() as $apple_touch_icon ) {
				$icon = Options::get( 'apple-touch-icon-' . $apple_touch_icon, null );
				if ( ! empty( $icon ) && ! empty( $icon['url'] ) ) {
					?>
					<link rel="apple-touch-icon" sizes="<?php echo esc_attr( $apple_touch_icon ); ?>" href="<?php echo esc_url( $icon['url'] ); ?>">
					<?php
				}
			}

			// Web App Enable.
			$web_app_capable = Options::get( 'web-app-capable', '0' );
			if ( '1' === $web_app_capable ) {
				?>
				<meta name="apple-mobile-web-app-capable" content="yes">
				<?php
				$web_app_status_bar_style = Options::get( 'web-app-status-bar-style', 'black' );
				?>
				<meta name="apple-mobile-web-app-status-bar-style" content="<?php echo esc_attr( $web_app_status_bar_style ); ?>">
				<?php
				$web_app_apple_mobile_title = Options::get( 'web-app-apple-mobile-web-app-title', '' );
				?>
				<meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( $web_app_apple_mobile_title ); ?>">
				<?php

				foreach ( Options::startup_screens() as $startup ) {
					$icon = Options::get( 'web-app-size-' . $startup['id'], null );
					if ( ! empty( $icon ) && ! empty( $icon['url'] ) ) {
						?>
						<link rel="apple-touch-startup-image" media="(device-width: <?php echo esc_attr( $startup['device-width'] ); ?>) and (device-height: <?php echo esc_attr( $startup['device-height'] ); ?>) and (-webkit-device-pixel-ratio: <?php echo esc_attr( $startup['device-pixel-ratio'] ); ?>)" href="<?php echo esc_url( $icon['url'] ); ?>">
						<?php
					}
				}
			}
		}
	}

	public function others() {
		if ( function_exists( 'Options::get' ) ) {

			// Web App Enable.
			$web_app_capable = Options::get( 'web-app-capable', '0' );
			if ( '1' === $web_app_capable ) {

				$manifest_options_array = array(
					'theme_color' => Options::get( 'web-app-theme-color', '#1e1e1e' ),
				);
				?>
				<meta name="theme-color" content="<?php echo esc_attr( $manifest_options_array['theme_color'] ); ?>">
				<?php

				global $wp_filesystem;
				if ( empty( $wp_filesystem ) ) {
					require_once ABSPATH . '/wp-admin/includes/file.php';
					WP_Filesystem();
				}

				$uploads_dir = $wp_filesystem->abspath() . '/wp-content/uploads';
				if ( $wp_filesystem->is_file( $uploads_dir . '/site.webmanifest' ) ) {
					?>
					<link rel="manifest" href="<?php echo esc_url( wp_get_upload_dir()['baseurl'] ); ?>/site.webmanifest">
					<?php
				}
			}
		}
	}

	public function manifest_related() {
		if ( function_exists( 'Options::get' ) ) {

			// Web App Enable.
			$web_app_capable = Options::get( 'web-app-capable', '0' );
			if ( '1' === $web_app_capable ) {

				$manifest_options_array = array(
					'theme_color'      => Options::get( 'web-app-theme-color', '#1e1e1e' ),
					'background_color' => Options::get( 'web-app-background-color', '#1e1e1e' ),
					'name'             => Options::get( 'web-app-application-name', 'Mobius Studio' ),
					'short_name'       => Options::get( 'web-app-application-name-short', 'Mobius Studio' ),
					'display'          => Options::get( 'web-app-display', 'standalone' ),
					'orientation'      => Options::get( 'web-app-orientation', 'portrait' ),
					'start_url'        => get_site_url(),
					'icons'            => array(),
				);

				foreach ( Options::chrome_icons() as $chrome_icon ) {
					$icon = Options::get( 'chrome-icon-' . $chrome_icon, null );

					if ( ! empty( $icon ) && ! empty( $icon['url'] ) ) {
						$manifest_options_array['icons'][] = array(
							'src'   => $icon['url'],
							'sizes' => $chrome_icon,
							'type'  => 'image/png',
						);
					}
				}

				global $wp_filesystem;
				if ( empty( $wp_filesystem ) ) {
					require_once ABSPATH . '/wp-admin/includes/file.php';
					WP_Filesystem();
				}

				$uploads_dir = $wp_filesystem->abspath() . '/wp-content/uploads';
				if ( ! $wp_filesystem->is_dir( $uploads_dir ) ) {
					if ( ! $wp_filesystem->mkdir( $uploads_dir ) ) {

						return false;

					}
				}

				$wp_filesystem->put_contents( $uploads_dir . '/site.webmanifest', wp_json_encode( $manifest_options_array ) );
			}
		}
		return false;
	}

	public function register_sw() {
		if ( function_exists( 'Options::get' ) ) {

			// Web App Enable.
			$web_app_capable = Options::get( 'web-app-capable', '0' );
			if ( '1' === $web_app_capable ) {
				?>
				<script type="text/javascript">
					if ('serviceWorker' in navigator) {
						navigator.serviceWorker.register('<?php echo esc_url( get_template_directory_uri() ); ?>/js/service-worker.js').then(function(registration) {
							console.log('ServiceWorker registration successful!');
						}).catch(function(err) {
							console.log('ServiceWorker registration failed: ', err);
						});
					}
				</script>
				<?php
			}
		}
		return false;
	}
}
