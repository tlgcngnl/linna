<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Typography {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => __( 'Typography', 'linna-framework-by-mobius-studio' ),
				'id'     => 'typography',
				'desc'   => __( 'Typography', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-fontsize',
				'fields' => array(
					array(
						'id'             => 'general-typography',
						'type'           => 'typography',
						'title'          => __( 'Body Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for everyting except logo, headings or menu.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'body', 'button', 'input', 'select', 'optgroup', 'textarea' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h1-typography',
						'type'           => 'typography',
						'title'          => __( 'H1 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h1' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h2-typography',
						'type'           => 'typography',
						'title'          => __( 'H2 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h2' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h3-typography',
						'type'           => 'typography',
						'title'          => __( 'H3 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h3' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h4-typography',
						'type'           => 'typography',
						'title'          => __( 'h4 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h4' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h5-typography',
						'type'           => 'typography',
						'title'          => __( 'H5 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h5' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
					array(
						'id'             => 'h6-typography',
						'type'           => 'typography',
						'title'          => __( 'H6 Typography', 'linna-framework-by-mobius-studio' ),
						'subtitle'       => __( 'Typography for title.', 'linna-framework-by-mobius-studio' ),
						'font-backup'    => true,
						'letter-spacing' => true,
						'text-transform' => true,
						'all_styles'     => false,
						'output'         => array( 'h6' ),
						'units'          => 'px',
						'default'        => array(),
						'fonts'          => array( '-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif' => 'System Fonts' ),
					),
				),
			),
		);
	}
}
