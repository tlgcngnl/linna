<?php

namespace Linna_Framework\Woocommerce;

use Linna_Framework\Woocommerce\Category_Icons\Category_Icons;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Class Woocommerce
 *
 * @package Linna_Framework\Woocommerce_Extend
 */
class Woocommerce_Extend {

	protected $sub_classes = array(
		Category_Icons::class,
	);

	function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Woocommerce (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Redux Framework installed and activated.
		if ( ! class_exists( 'WooCommerce' ) || ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_missing_main_plugin' ) );

			return;
		}

		foreach ( $this->sub_classes as $class ) {
			( new $class() );
		}

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Woocommerce installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( ( wp_verify_nonce( 'woocommerce' ) || wp_verify_nonce( 'linna-framework-woocommerce' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: Redux Framework */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'Woocommerce', 'linna-framework-by-mobius-studio' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}



}

