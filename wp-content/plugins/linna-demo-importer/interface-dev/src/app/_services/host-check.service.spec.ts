import { TestBed } from '@angular/core/testing';

import { HostCheckService } from './host-check.service';

describe('HostCheckService', () => {
  let service: HostCheckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HostCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
