<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class General_Settings {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'General Settings', 'linna-framework-by-mobius-studio' ),
				'id'     => 'general-settings',
				'desc'   => esc_html__( 'Custom Codes ', 'linna-framework-by-mobius-studio' ),
				'icon'   => 'el el-cogs',
				'fields' => array(
					array(
						'id'       => 'header_css',
						'type'     => 'ace_editor',
						'title'    => __( 'Header CSS Code', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'CSS Code to place right before </head> tag. Beware, this code will be html escaped.', 'linna-framework-by-mobius-studio' ),
						'mode'     => 'css',
						'theme'    => 'monokai',
						'default'  => '',
					),
					array(
						'id'       => 'google_anaytics_id',
						'type'     => 'text',
						'title'    => esc_html__( 'Google Analytics', 'linna-framework-by-mobius-studio' ),
						'subtitle' => esc_html__( 'UA-XXXXX-Y', 'linna-framework-by-mobius-studio' ),
					),
					array(
						'id'       => 'body-background',
						'type'     => 'color_gradient',
						'title'    => __( 'Body Background', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Behind the content area.', 'linna-framework-by-mobius-studio' ),
						'output'   => array( '#virtual-body' ),
						'validate' => 'color',
						'default'  => array(
							'from' => '',
							'to'   => '',
						),
					),
					array(
						'id'       => 'page-background',
						'type'     => 'background',
						'title'    => __( 'Page Background', 'linna-framework-by-mobius-studio' ),
						'subtitle' => __( 'Content background color.', 'linna-framework-by-mobius-studio' ),
						'output'   => array( '#page' ),
					),
				),
			),
		);
	}
}
