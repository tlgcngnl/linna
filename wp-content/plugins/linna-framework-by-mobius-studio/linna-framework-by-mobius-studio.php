<?php
/**
 * Plugin Name: Linna Framework by Mobius Studio
 * Description: Extending theme functionality.
 * Author: Mobius Studio
 * Version: 1.0.0
 * Author URI: https://mobius.studio
 *
 * Text Domain: linna-framework-by-mobius-studio
 *
 * @package Linna Framework by Mobius Studio
 * @category Core
 */

use Linna_Framework\Admin\Menu;
use Linna_Framework\Admin\Security;
use Linna_Framework\Elementor\Elementor;
use Linna_Framework\Menu\Menu_Icons;
use Linna_Framework\Options\Options;
use Linna_Framework\WebApp\WebApp;
use Linna_Framework\Widgets\Widgets;
use Linna_Framework\Woocommerce\Woocommerce_Extend;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'LINNA_FRAMEWORK_VERSION', '1.0.0' );
define( 'LINNA_FRAMEWORK__FILE__', __FILE__ );
define( 'LINNA_FRAMEWORK_PLUGIN_BASE', plugin_basename( LINNA_FRAMEWORK__FILE__ ) );
define( 'LINNA_FRAMEWORK_PATH', plugin_dir_path( LINNA_FRAMEWORK__FILE__ ) );
define( 'LINNA_FRAMEWORK_URL', plugins_url( '/', LINNA_FRAMEWORK__FILE__ ) );
define( 'LINNA_FRAMEWORK_ASSETS_URL', LINNA_FRAMEWORK_URL . 'assets/' );

require_once LINNA_FRAMEWORK_PATH . 'vendor/autoload.php';

new Security();
new Menu();
new Widgets();
new Menu_Icons();
new Elementor();
new Options();
new WebApp();
new Woocommerce_Extend();
