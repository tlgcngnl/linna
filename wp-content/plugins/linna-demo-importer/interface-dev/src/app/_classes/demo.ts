export class Demo {
	name: string;
	description?: string;
	media?: any[];
	sql:{
		zip?:string;
		imp?:string;
		options?:string;
	};
	preview: string;
}
