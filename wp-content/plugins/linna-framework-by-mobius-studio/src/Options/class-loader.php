<?php

namespace Linna_Framework\Options;

use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Loader {
	public function __construct() {
		add_action( sprintf( 'redux/extensions/%s/before', Options::$opt_name ), array( $this, 'linna_framework_redux_register_custom_extension_loader' ), 0 );
	}

	/**
	 * Loader for all custom extensions in extensions folder.
	 *
	 * @param Redux $redux_framework Redux base class.
	 */
	public function linna_framework_redux_register_custom_extension_loader( $redux_framework ) {
		$path    = dirname( __FILE__ ) . '/Extensions/';
		$folders = scandir( $path, 1 );
		foreach ( $folders as $folder ) {
			if ( '.' === $folder || '..' === $folder || ! is_dir( $path . $folder ) ) {
				continue;
			}
			$extension_class = 'ReduxFramework_Extension_' . $folder;
			if ( ! class_exists( $extension_class ) ) {
				// In case you wanted override your override, hah.
				$class_file = $path . $folder . '/extension_' . $folder . '.php';
				$class_file = apply_filters( 'redux/extension/' . $redux_framework->args['opt_name'] . '/' . $folder, $class_file );
				if ( $class_file ) {
					require_once $class_file;
				}
			}
			if ( ! isset( $redux_framework->extensions[ $folder ] ) ) {
				$redux_framework->extensions[ $folder ] = new $extension_class( $redux_framework );
			}
		}
	}
}
