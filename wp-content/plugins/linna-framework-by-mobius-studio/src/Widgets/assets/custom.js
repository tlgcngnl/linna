/**
 * Custom scripts for widgets management.
 *
 * Global jQuery
 *
 * @package Linna_Widgets
 * @version 1.0.0
 */

( function( $ ){
	function initColorPicker( widget ) {
		var el = widget.find( '.color-picker-pro:not([id*="__i__"])' );
		var id = el.attr( "id" );
		if ( id ) {
			var colorpickerInsideInputAnchorLeft  = $( '#' + id + '-anchor' ).find( '[data-color]' );
			var colorPickerDefaultInsideInputLeft = new ColorPicker.Default(
				'#' + id,
				{
					color: el.val()
				}
			);
			colorpickerInsideInputAnchorLeft.css( 'background', colorPickerDefaultInsideInputLeft.getColor() );
			colorPickerDefaultInsideInputLeft.on(
				'change',
				function(color) {
					colorpickerInsideInputAnchorLeft.css( 'background', color );

					el.parents( 'form' ).find( '[type="submit"]' ).prop( 'disabled', false );
				}
			);
		}
	}
	function initNoUiSlider( widget ) {
		var sliders = widget.find( '.nouislider' ).not( '[id*="__i__"]' );

		sliders.each(
			function () {
				var range = this;

				noUiSlider.create(
					range,
					{
						start: [ range.dataset.start ],
						step: parseInt( range.dataset.step ),
						connect: [true, false],
						range: {
							'min': parseInt( range.dataset.min ),
							'max': parseInt( range.dataset.max )
						}
					}
				);

				$( range ).addClass( 'initialized' );

				var bases = $( range ).find( '.noUi-base' );
				if (bases.length > 1) {
					bases.first().remove();
				}

				var valueInput = document.getElementById( range.dataset.of ),
				valueSpan      = document.getElementById( 'content-' + range.dataset.of );

				// When the slider value changes, update the input and span.
				range.noUiSlider.on(
					'update',
					function( values, handle ) {
						valueInput.value    = parseInt( values[handle] );
						valueSpan.innerText = parseInt( values[handle] ).toString();

						$( range ).parents( 'form' ).find( '[type="submit"]' ).prop( 'disabled', false );
					}
				);

				// When the input changes, set the slider value.
				valueInput.addEventListener(
					'change',
					function(){
						range.noUiSlider.set( [null, this.value] );
					}
				);
			}
		);
	}

	function onFormUpdate( event, widget ) {
		initColorPicker( widget );
		initNoUiSlider( widget );
	}

	$( document ).on( 'widget-added widget-updated', onFormUpdate );
	var last_clicked_media_library_button = null;

	var customMediaLibrary = window.wp.media(
		{

			// Accepts [ 'select', 'post', 'image', 'audio', 'video' ]
			// Determines what kind of library should be rendered.
			frame: 'select',

			// Modal title.
			title: "'Select Images',",

			// Enable/disable multiple select.
			multiple: false,

			// Library wordpress query arguments.
			library: {
				order: 'DESC',

				// [ 'name', 'author', 'date', 'title', 'modified', 'uploadedTo', 'id', 'post__in', 'menuOrder' ]
				orderby: 'date',

				// mime type. e.g. 'image', 'image/jpeg'.
				type: 'image',

				// Searches the attachment title.
				search: null,

				// Includes media only uploaded to the specified post (ID).
				uploadedTo: null // wp.media.view.settings.post.id (for current post ID).
			},

			button: {
				text: 'Done'
			}

		}
	);
	$( document ).ready(
		function() {
			$( '.widget-inside:has(.color-picker-pro), .widget-inside:has(.nouislider)' ).each(
				function () {
					initColorPicker( $( this ) );
					initNoUiSlider( $( this ) );
				}
			);

			$( document ).on(
				'click',
				'.linna-widgets-image-picker',
				function (e) {
					e.preventDefault();
					last_clicked_media_library_button = $( this );
					customMediaLibrary.open();
				}
			);

			$( document ).on(
				'click',
				'.linna-widgets-image-unpicker',
				function (e) {
					e.preventDefault();
					var target       = $( this ).data( 'of' );
					var target_input = $( '#' + target );
					var preview      = $( '#preview-' + target );

					target_input.val( '' );
					preview.attr( 'src', '' );

					$( this ).parents( 'form' ).find( '[type="submit"]' ).prop( 'disabled', false );
				}
			);

			customMediaLibrary.on(
				'open',
				function() {

					// Assuming the post IDs will be fetched from db.
					var target          = last_clicked_media_library_button.data( 'of' );
					var target_input    = $( '#' + target );
					var selectedPostIDs = [ target_input.val() ];

					// Used for defining the image selections in the media library.
					var selectionAPI = customMediaLibrary.state().get( 'selection' );

					selectedPostIDs.forEach(
						function( imageID ) {
							var attachment = wp.media.attachment( imageID );
							selectionAPI.add( attachment ? [ attachment ] : [] );
						}
					);

				}
			);

			customMediaLibrary.on(
				'select',
				function() {

					// write your handling code here.
					var selectedImages = customMediaLibrary.state().get( 'selection' );
					if ( selectedImages.length ) {
						var attachment   = selectedImages.first().toJSON();
						var target       = last_clicked_media_library_button.data( 'of' );
						var target_input = $( '#' + target );
						var preview      = $( '#preview-' + target );

						target_input.val( attachment.id );
						preview.attr( 'src', attachment.url );
					}

					last_clicked_media_library_button.parents( 'form' ).find( '[type="submit"]' ).prop( 'disabled', false );

					// Probably send the image IDs to the backend using ajax?
				}
			);
		} 
	);

}( jQuery ) );
