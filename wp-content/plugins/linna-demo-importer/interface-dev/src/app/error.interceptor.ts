import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, delay, mergeMap, retryWhen} from 'rxjs/operators';

const DEFAULT_MAX_RETRIES = 2;
const DEFAULT_BACKOFF = 1000;

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
				delay(1000),
                this.retryWithBackoff(1000),
            );
    }

    retryWithBackoff(delayMs: number, maxRetry = DEFAULT_MAX_RETRIES, backoffMS = DEFAULT_BACKOFF) {
        let retries = maxRetry;

        return (src: Observable<any>) =>
            src.pipe(
                retryWhen((errors:Observable<any>) => errors.pipe(
                    mergeMap(error => {
                        // console.log(error);
                        // console.log(retries);
                        // retries -= 1;
                        if(retries-- > 0){
                            const backoffTime = delayMs + (maxRetry - retries) * backoffMS;
                            return of(error).pipe(delay(backoffTime))
                        }

                        if( retries < 0 ){
                            // console.log('THROWS HERE', error);
                            if (!error?.error?.message){
								error.error = {
									message: "Network or connection error."
								};
							}
                            // error.error.message = "Bağlantı hatası oluştu.";
                            return throwError(error);
                        }

                        // console.log('THROWS AFTER', error);
                        return throwError(error);
                    })
                ))
            )
    }
}
