<?php

namespace Linna_Framework\Woocommerce\Category_Icons;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS__FILE__', __FILE__ );
define( 'LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS_PATH', plugin_dir_path( LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS__FILE__ ) );
define( 'LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS_URL', plugins_url( '/', LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS__FILE__ ) );

class Category_Icons {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	function __construct() {

		// Add custom fields to create and edit forms.
		add_action( 'product_cat_add_form_fields', array( $this, 'taxonomy_add_new_category_icon_meta_field' ), 10, 1 );
		add_action( 'product_cat_edit_form_fields', array( $this, 'taxonomy_edit_category_icon_meta_field' ), 10, 1 );

		// Manage create and edit savings.
		add_action( 'edited_product_cat', array( $this, 'save_taxonomy_category_icon_meta' ), 10, 1 );
		add_action( 'create_product_cat', array( $this, 'save_taxonomy_category_icon_meta' ), 10, 1 );

		// Add columns to list table.
		add_filter( 'manage_edit-product_cat_columns', array( $this, 'product_cat_columns' ) );
		add_filter( 'manage_product_cat_custom_column', array( $this, 'product_cat_column' ), 10, 3 );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ), 10, 1 );

	}

	/**
	 * Enqueue custom required css and javascripts.
	 *
	 * @param string $hook Current page.
	 *
	 * @access      public
	 * @return      void
	 * @since       1.0
	 */
	public function enqueues( $hook ) {

		if ( 'edit-tags.php' !== $hook ) {
			return;
		}

		wp_enqueue_style( 'linna-framework-woocommerce-category-icons', LINNA_FRAMEWORK_WOOCOMMERCE_CATEGORY_ICONS_URL . 'category-icons.css', array(), self::VERSION );

	}

	public function taxonomy_add_new_category_icon_meta_field() {

		?>

		<div class="form-field">
			<label for="linna_wc_category_icon"><?php esc_html_e( 'Select Icon', 'linna-framework-by-mobius-studio' ); ?></label>
			<input type="text" name="linna_wc_category_icon" id="linna_wc_category_icon">
			<p class="description"><?php esc_html_e( 'Select an icon for this category.', 'linna-framework-by-mobius-studio' ); ?></p>
		</div>

		<?php
	}

	public function taxonomy_edit_category_icon_meta_field( $term ) {

		$term_id = $term->term_id;

		$linna_wc_category_icon = get_term_meta( $term_id, 'linna_wc_category_icon', true );

		?>

		<tr class="form-field">
			<th scope="row"><label for="linna_wc_category_icon"><?php esc_html_e( 'Select Icon', 'linna-framework-by-mobius-studio' ); ?></label></th>
			<td>
				<input type="text" name="linna_wc_category_icon" id="linna_wc_category_icon" value="<?php echo esc_attr( $linna_wc_category_icon ) ? esc_attr( $linna_wc_category_icon ) : ''; ?>">
				<p class="description"><?php esc_html_e( 'Select an icon for this category.', 'linna-framework-by-mobius-studio' ); ?></p>
			</td>
		</tr>

		<?php
	}

	public function save_taxonomy_category_icon_meta( $term_id ) {

		$linna_wc_category_icon = filter_input( INPUT_POST, 'linna_wc_category_icon' );

		update_term_meta( $term_id, 'linna_wc_category_icon', $linna_wc_category_icon );

	}

	/**
	 * Icon column added to category admin.
	 *
	 * @param mixed $columns Columns array.
	 * @return array
	 */
	public function product_cat_columns( $columns ) {
		$new_columns = array();

		if ( isset( $columns['cb'] ) ) {
			$new_columns['cb'] = $columns['cb'];
			unset( $columns['cb'] );
		}

		$new_columns['icon'] = __( 'Icon', 'linna-framework-by-mobius-studio' );

		$columns           = array_merge( $new_columns, $columns );
		$columns['handle'] = '';

		return $columns;
	}

	/**
	 * Icon column value added to category admin.
	 *
	 * @param string $columns Column HTML output.
	 * @param string $column Column name.
	 * @param int    $id Product ID.
	 *
	 * @return string
	 */
	public function product_cat_column( $columns, $column, $id ) {

		if ( 'icon' === $column ) {

			$svg = get_term_meta( $id, 'linna_wc_category_icon', true );

			$columns .= $svg;

		}

		return $columns;
	}


}
