<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="linna-framework-product-categories">
	<?php
	foreach ( $items as $item ) :
		echo wp_kses( $item, linna_get_kses_extended_ruleset() );
	endforeach;
	?>
</div>
