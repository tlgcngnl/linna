<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Favicons {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'Favicons & Apple Icons', 'linna-framework-by-mobius-studio' ),
				'desc'   => esc_html__( 'Favicons are handled by the Site Icon setting in the customizer since version 4.3.', 'linna-framework-by-mobius-studio' ),
				'id'     => 'favicons',
				'icon'   => 'el el-idea',
				'fields' => array_merge(
					array_map(
						function ( $size ) {
							return array(
								'id'          => 'apple-touch-icon-' . $size,
								'type'        => 'media',
								'url'         => true,
								'title'       => __( 'Apple Touch Icon .PNG ', 'linna-framework-by-mobius-studio' ) . ' ' . $size,
								'description' => __( 'Transparent or filled PNG image.', 'linna-framework-by-mobius-studio' ),
								'compiler'    => 'true',
							);
						},
						Options::apple_touch_icons()
					),
					array_map(
						function ( $size ) {
							return array(
								'id'          => 'chrome-icon-' . $size,
								'type'        => 'media',
								'url'         => true,
								'title'       => __( 'Chrome Icon .PNG ', 'linna-framework-by-mobius-studio' ) . ' ' . $size,
								'description' => __( 'Transparent or filled PNG image.', 'linna-framework-by-mobius-studio' ),
								'compiler'    => 'true',
							);
						},
						Options::chrome_icons()
					),
				),
			),
		);
	}
}
