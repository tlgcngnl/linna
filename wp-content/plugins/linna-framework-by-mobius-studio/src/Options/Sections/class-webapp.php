<?php

namespace Linna_Framework\Options\Sections;

use Linna_Framework\Options\Options;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Webapp {
	public function __construct() {
		Redux::setSection(
			Options::$opt_name,
			array(
				'title'  => esc_html__( 'Web App', 'linna-framework-by-mobius-studio' ),
				'id'     => 'webapp',
				'icon'   => 'el el-screen-alt',
				'desc'   => sprintf(
					'<a href="%1$s" target="_blank">%2$s</a>',
					esc_url( 'https://appsco.pe/developer/splash-screens' ),
					__( 'You can easily create your splash screens from this page, download then upload here. This website is not related to Mobius Studio by any means.', 'linna-framework-by-mobius-studio' )
				),
				'fields' =>
					array_merge(
						array(
							array(
								'id'      => 'web-app-capable',
								'type'    => 'select',
								'title'   => __( 'WebApp Capable', 'linna-framework-by-mobius-studio' ),
								'options' => array(
									'1' => esc_html__( 'Enabled', 'linna-framework-by-mobius-studio' ),
									'0' => esc_html__( 'Disabled', 'linna-framework-by-mobius-studio' ),
								),
								'default' => '0',
							),
							array(
								'id'      => 'web-app-status-bar-style',
								'type'    => 'select',
								'title'   => __( 'Status Bar Style', 'linna-framework-by-mobius-studio' ),
								'options' => array(
									'black'             => esc_html__( 'Black', 'linna-framework-by-mobius-studio' ),
									'black-translucent' => esc_html__( 'Black Translucent', 'linna-framework-by-mobius-studio' ),
								),
								'default' => 'black',
							),
							array(
								'id'      => 'web-app-application-name',
								'type'    => 'text',
								'title'   => __( 'Application Name', 'linna-framework-by-mobius-studio' ),
								'default' => 'Mobius Studio',
							),
							array(
								'id'      => 'web-app-application-name-short',
								'type'    => 'text',
								'title'   => __( 'Application Name (Short)', 'linna-framework-by-mobius-studio' ),
								'default' => 'Mobius Studio',
							),
							array(
								'id'          => 'web-app-theme-color',
								'type'        => 'color',
								'title'       => __( 'Theme Color', 'linna-framework-by-mobius-studio' ),
								'transparent' => false,
								'default'     => '#1e1e1e',
							),
							array(
								'id'          => 'web-app-background-color',
								'type'        => 'color',
								'title'       => __( 'Background Color', 'linna-framework-by-mobius-studio' ),
								'transparent' => false,
								'default'     => '#1e1e1e',
							),
							array(
								'id'      => 'web-app-display',
								'type'    => 'select',
								'title'   => __( 'WebApp Display Mode', 'linna-framework-by-mobius-studio' ),
								'options' => array(
									'fullscreen' => esc_html__( 'Fullscreen', 'linna-framework-by-mobius-studio' ),
									'standalone' => esc_html__( 'Standalone (Native App Like)', 'linna-framework-by-mobius-studio' ),
									'minimal-ui' => esc_html__( 'Minimal UI', 'linna-framework-by-mobius-studio' ),
									'browser'    => esc_html__( 'Browser', 'linna-framework-by-mobius-studio' ),
								),
								'default' => 'standalone',
							),
							array(
								'id'      => 'web-app-orientation',
								'type'    => 'select',
								'title'   => __( 'Preferred Orientation', 'linna-framework-by-mobius-studio' ),
								'options' => array(
									'any'                 => esc_html__( 'Any', 'linna-framework-by-mobius-studio' ),
									'natural'             => esc_html__( 'Natural', 'linna-framework-by-mobius-studio' ),
									'landscape'           => esc_html__( 'Landscape', 'linna-framework-by-mobius-studio' ),
									'landscape-primary'   => esc_html__( 'Landscape Primary', 'linna-framework-by-mobius-studio' ),
									'landscape-secondary' => esc_html__( 'Landscape Secondary', 'linna-framework-by-mobius-studio' ),
									'portrait'            => esc_html__( 'Portrait', 'linna-framework-by-mobius-studio' ),
									'portrait-primary'    => esc_html__( 'Portrait Primary', 'linna-framework-by-mobius-studio' ),
									'portrait-secondary'  => esc_html__( 'Portrait Secondary', 'linna-framework-by-mobius-studio' ),
								),
								'default' => 'portrait',
							),
						),
						array_map(
							function ( $size ) {
								return array(
									'id'          => 'web-app-size-' . $size['id'],
									'type'        => 'media',
									'url'         => true,
									'title'       => $size['title'],
									'description' => __( 'StartUp/Splash Screen', 'linna-framework-by-mobius-studio' ),
									'compiler'    => 'true',
								);
							},
							Options::startup_screens()
						),
					),
			),
		);
	}
}
