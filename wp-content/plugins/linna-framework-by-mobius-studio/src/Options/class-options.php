<?php

namespace Linna_Framework\Options;

use Linna_Framework\Options\Sections\Blog;
use Linna_Framework\Options\Sections\Favicons;
use Linna_Framework\Options\Sections\Footer;
use Linna_Framework\Options\Sections\General_Settings;
use Linna_Framework\Options\Sections\Header;
use Linna_Framework\Options\Sections\Loading_Screen;
use Linna_Framework\Options\Sections\Logo;
use Linna_Framework\Options\Sections\Menu;
use Linna_Framework\Options\Sections\Page_Not_Found;
use Linna_Framework\Options\Sections\Sidebar;
use Linna_Framework\Options\Sections\Typography;
use Linna_Framework\Options\Sections\Webapp;
use Redux;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'LINNA_FRAMEWORK_OPTIONS__FILE__', __FILE__ );
define( 'LINNA_FRAMEWORK_OPTIONS_PATH', plugin_dir_path( LINNA_FRAMEWORK_OPTIONS__FILE__ ) );
define( 'LINNA_FRAMEWORK_OPTIONS_URL', plugins_url( '/', LINNA_FRAMEWORK_OPTIONS__FILE__ ) );
define( 'LINNA_FRAMEWORK_OPTIONS_ASSETS_URL', LINNA_FRAMEWORK_OPTIONS_URL . 'assets/' );

class Options {

	public static $opt_name = 'linna_theme_options';

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Redux (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Redux Framework installed and activated.
		if ( ! class_exists( 'Redux' ) || ! is_plugin_active( 'redux-framework/redux-framework.php' ) ) {
			add_action( 'admin_notices', array( $this, 'admin_notice_missing_main_plugin' ) );

			return;
		}

		// Load custom redux extensions.
		new Loader();

		// Set Basic Args.
		self::set_args();

		// Load all sections.
		self::load_sections();
	}

	/**
	 * Get option.
	 *
	 * @param string|array $name Redux option id.
	 * @param bool|string  $default Optional default value, if option is not set.
	 * @param bool         $check_with_empty Check value with empty and return default value according to it.
	 *
	 * @return bool|mixed|void|null
	 */
	public static function get( $name, $default = false, $check_with_empty = false ) {
		if ( is_customize_preview() ) {
			$options = $GLOBALS[ self::$opt_name ];
		} else {
			$options = get_option( self::$opt_name );
		}

		if ( is_array( $name ) ) {
			foreach ( $name as $item ) {
				if ( isset( $options[ $item ] ) ) {
					$options = $options[ $item ];
				} else {
					$options = null;
					break;
				}
			}
		} else {
			if ( isset( $options[ $name ] ) ) {
				$options = $options[ $name ];
			} else {
				$options = null;
			}
		}

		if ( ( $check_with_empty && empty( $options ) && isset( $default ) ) || ( is_null( $options ) && isset( $default ) ) ) {
			return $default;
		}

		return $options;
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Redux Framework installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( ( wp_verify_nonce( 'redux' ) || wp_verify_nonce( 'linna-framework-options' ) ) && isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
		/* translators: 1: Plugin name 2: Redux Framework */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'linna-framework-by-mobius-studio' ),
			'<strong>' . esc_html__( 'Linna Framework', 'linna-framework-by-mobius-studio' ) . '</strong>',
			'<strong>' . esc_html__( 'Redux Framework', 'linna-framework-by-mobius-studio' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', esc_html( $message ) );

	}

	public function set_args() {
		$theme = wp_get_theme();

		$args = array(
			// TYPICAL -> Change these values as you need/desire.
			'opt_name'             => self::$opt_name,
			// This is where your data is stored in the database and also becomes your global variable name.
			'display_name'         => $theme->get( 'Name' ),
			// Name that appears at the top of your panel.
			'display_version'      => $theme->get( 'Version' ),
			// Version that appears at the top of your panel.
			'menu_type'            => 'submenu',
			// Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only).
			'allow_sub_menu'       => true,
			// Show the sections below the admin menu item or not.
			'menu_title'           => esc_html__( 'Theme Options', 'linna-framework-by-mobius-studio' ),
			'page_title'           => esc_html__( 'Theme Options', 'linna-framework-by-mobius-studio' ),
			// You will need to generate a Google API key to use this feature.
			// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth.
			'google_api_key'       => '',
			// Set it you want google fonts to update weekly. A google_api_key value is required.
			'google_update_weekly' => false,
			// Must be defined to add google fonts to the typography module.
			'async_typography'     => false,
			// Use a asynchronous font on the front end or font string.
			// 'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader.
			'admin_bar'            => true,
			// Show the panel pages on the admin bar.
			'admin_bar_icon'       => 'dashicons-admin-generic',
			// Choose an icon for the admin bar menu.
			'admin_bar_priority'   => 50,
			// Choose an priority for the admin bar menu.
			'global_variable'      => '',
			// Set a different name for your global variable other than the opt_name.
			'dev_mode'             => true,
			// Show the time the page took to load, lcb.
			'update_notice'        => true,
			// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo.
			'customizer'           => true,
			// Enable basic customizer support.
			// 'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
			// 'disable_save_warn' => true,                    // Disable the save warning when a user changes a field.
			'show_options_object'  => false,
			// OPTIONAL -> Give you extra features.
			'page_priority'        => null,
			// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
			'page_parent'          => $theme->get( 'TextDomain' ),
			// For a full list of options, visit: //codex.wordpress.org/Function_Reference/add_submenu_page#Parameters.
			'page_permissions'     => 'manage_options',
			// Permissions needed to access the options panel.
			'menu_icon'            => '',
			// Specify a custom URL to an icon.
			'last_tab'             => '',
			// Force your panel to always open to a specific tab (by id).
			'page_icon'            => 'icon-themes',
			// Icon displayed in the admin panel next to your menu_title.
			'page_slug'            => 'theme-options',
			// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided.
			'save_defaults'        => true,
			// On load save the defaults to DB before user clicks save or not.
			'default_show'         => false,
			// If true, shows the default value next to each field that is not the default value.
			'default_mark'         => '',
			// What to print by the field's title if the value shown is default. Suggested: *.
			'show_import_export'   => true,
			// Shows the Import/Export panel when not used as a field.

			// CAREFUL -> These options are for advanced use only.
			'transient_time'       => 60 * MINUTE_IN_SECONDS,
			'output'               => true,
			// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output.
			'output_tag'           => true,
			// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
			// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

			// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
			'database'             => '',
			// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
			'use_cdn'              => true,
			// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

			// HINTS.
			'hints'                => array(
				'icon'          => 'el el-question-sign',
				'icon_position' => 'right',
				'icon_color'    => 'lightgray',
				'icon_size'     => 'normal',
				'tip_style'     => array(
					'color'   => 'red',
					'shadow'  => true,
					'rounded' => false,
					'style'   => '',
				),
				'tip_position'  => array(
					'my' => 'top left',
					'at' => 'bottom right',
				),
				'tip_effect'    => array(
					'show' => array(
						'effect'   => 'slide',
						'duration' => '500',
						'event'    => 'mouseover',
					),
					'hide' => array(
						'effect'   => 'slide',
						'duration' => '500',
						'event'    => 'click mouseleave',
					),
				),
			),
		);

		Redux::setArgs( self::$opt_name, $args );
	}

	public function load_sections() {
		new General_Settings();
		new Typography();
		new Header();
		new Logo();
		new Favicons();
		new Webapp();
		new Sidebar();
		new Menu();
		new Blog();
		new Page_Not_Found();
		new Footer();
		new Loading_Screen();
	}

	/**
	 * Icon sizes
	 */
	public static function icos() {
		return array( '16x16' );
	}

	/**
	 * Icon sizes
	 */
	public static function favicons() {
		return array( '16x16', '32x32' );
	}

	/**
	 * Icon sizes
	 */
	public static function apple_touch_icons() {
		return array( '180x180' );
	}

	/**
	 * Icon sizes
	 */
	public static function chrome_icons() {
		return array( '192x192', '512x512' );
	}

	/**
	 * Icon sizes
	 */
	public static function startup_screens() {
		return array(
			array(
				'id'                 => 'iphone-x',
				'title'              => __( 'iPhone X (1125px x 2436px)', 'linna' ),
				'device-width'       => '375px',
				'device-height'      => '812px',
				'device-pixel-ratio' => '3',
			),
			array(
				'id'                 => 'iphone-8',
				'title'              => __( 'iPhone 8, 7, 6s, 6 (750px x 1334px)', 'linna' ),
				'device-width'       => '375px',
				'device-height'      => '667px',
				'device-pixel-ratio' => '2',
			),
			array(
				'id'                 => 'iphone-plus',
				'title'              => __( 'iPhone 8 Plus, 7 Plus, 6s Plus, 6 Plus (1242px x 2208px)', 'linna' ),
				'device-width'       => '414px',
				'device-height'      => '736px',
				'device-pixel-ratio' => '3',
			),
			array(
				'id'                 => 'iphone-5',
				'title'              => __( 'iPhone 5 (640px x 1136px)', 'linna' ),
				'device-width'       => '320px',
				'device-height'      => '568px',
				'device-pixel-ratio' => '2',
			),
			array(
				'id'                 => 'ipad-mini-air',
				'title'              => __( 'iPad Mini, Air (1536px x 2048px)', 'linna' ),
				'device-width'       => '768px',
				'device-height'      => '1024px',
				'device-pixel-ratio' => '2',
			),
			array(
				'id'                 => 'ipad-pro-10-5',
				'title'              => __( 'iPad Pro 10.5" (1668px x 2224px)', 'linna' ),
				'device-width'       => '834px',
				'device-height'      => '1112px',
				'device-pixel-ratio' => '2',
			),
			array(
				'id'                 => 'ipad-pro-12-9',
				'title'              => __( 'iPad Pro 12.9" (2048px x 2732px)', 'linna' ),
				'device-width'       => '1024px',
				'device-height'      => '1366px',
				'device-pixel-ratio' => '2',
			),

		);
	}
}
