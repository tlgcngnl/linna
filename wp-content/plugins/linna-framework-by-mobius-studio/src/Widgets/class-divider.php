<?php

namespace Linna_Framework\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use WP_Widget;

/**
 * Class Divider
 *
 * @package Linna_Framework\Widgets
 */
class Divider extends WP_Widget {

	/**
	 * @var string[]
	 */
	public static $defaults = array(
		'width'   => 100,
		'height'  => 1,
		'color'   => '',
		'spacing' => 10,
	);

	/**
	 * Divider_Widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'Divider',
			'description' => 'Seperate your widgets with style.',
		);
		parent::__construct( 'Divider', 'Divider Widget', $widget_ops );

		add_action( 'admin_enqueue_scripts', array( $this, 'widget_backend_scripts' ), 0 );
	}

	public function widget_backend_scripts() {
		/*wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker-alpha', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'libs/wp-color-picker-alpha/wp-color-picker-alpha.min.js', array( 'wp-color-picker' ), '1.0.0', true );*/

		wp_enqueue_style( 'color-picker-pro', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'libs/color-picker-pro/light.min.css', array(), '1.0.0' );
		wp_enqueue_script( 'color-picker-pro', LINNA_FRAMEWORK_WIDGETS_ASSETS_URL . 'libs/color-picker-pro/default-picker.min.js', array(), '1.0.0', true );
	}

	public function form( $instance ) {
		$instance = wp_parse_args(
			(array) $instance,
			self::$defaults,
		);

		?>
		<div class="pagebox">
			<p><?php _e( 'Color', 'linna-framework-by-mobius-studio' ); ?></p>
			<span class="colorpicker-input colorpicker-input--position-left">
				<input class="color-picker-pro" id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>" value="<?php echo esc_attr( $instance['color'] ); ?>" type="text">
				<span id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>-anchor" class="colorpicker-custom-anchor colorpicker-circle-anchor">
					<span class="colorpicker-circle-anchor__color" data-color></span>
				</span>
			</span>
		</div>
		<div class="pagebox">
			<p><?php _e( 'Width', 'linna-framework-by-mobius-studio' ); ?> <strong id="content-<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>"></strong>%</p>
			<div class="nouislider" data-of="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>" data-min="0" data-max="100" data-start="<?php echo esc_attr( $instance['width'] ); ?>" data-step="1"></div>
			<input id="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>" type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'width' ) ); ?>" value="<?php echo esc_attr( $instance['width'] ); ?>"/>
		</div>
		<div class="pagebox">
			<p><?php _e( 'Height', 'linna-framework-by-mobius-studio' ); ?> <strong id="content-<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"></strong>px</p>
			<div class="nouislider" data-of="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>" data-min="0" data-max="100" data-start="<?php echo esc_attr( $instance['height'] ); ?>" data-step="1"></div>
			<input id="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>" type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'height' ) ); ?>" value="<?php echo esc_attr( $instance['height'] ); ?>"/>
		</div>
		<div class="pagebox">
			<p><?php _e( 'Spacing', 'linna-framework-by-mobius-studio' ); ?> <strong id="content-<?php echo esc_attr( $this->get_field_id( 'spacing' ) ); ?>"></strong>px</p>
			<div class="nouislider" data-of="<?php echo esc_attr( $this->get_field_id( 'spacing' ) ); ?>" data-min="0" data-max="100" data-start="<?php echo esc_attr( $instance['spacing'] ); ?>" data-step="1"></div>
			<input id="<?php echo esc_attr( $this->get_field_id( 'spacing' ) ); ?>" type="hidden" name="<?php echo esc_attr( $this->get_field_name( 'spacing' ) ); ?>" value="<?php echo esc_attr( $instance['spacing'] ); ?>"/>
		</div>

		<div style="height: 30px"></div>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		foreach ( self::$defaults as $key => $arg ) {
			$instance[ $key ] = $new_instance[ $key ];
		}

		return $instance;
	}

	function widget( $args, $instance ) {

		$css = array();

		foreach ( array_keys( self::$defaults ) as $property ) {
			if ( empty( $instance[ $property ] ) ) {
				continue;
			}

			switch ( $property ) {
				case 'spacing':
					$css[] = sprintf( 'margin-top: %spx;', $instance[ $property ] );
					$css[] = sprintf( 'margin-bottom: %spx;', $instance[ $property ] );
					break;
				case 'width':
					$css[] = sprintf( '%s: %s%%;', $property, $instance[ $property ] );
					break;
				case 'height':
					$css[] = sprintf( '%s: %spx;', $property, $instance[ $property ] );
					break;
				case 'color':
					$css[] = sprintf( 'background-%s: %s;', $property, $instance[ $property ] );
					break;
			}
		}

		?>
		<div style="<?php echo esc_attr( join( $css ) ); ?>"></div>
		<?php
	}
}

