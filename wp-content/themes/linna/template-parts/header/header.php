<?php
/**
 * Displays header
 *
 * @package WordPress
 * @subpackage Linna
 * @since 1.0.0
 */
?>
<div class="site-row">

	<?php if ( linna_option( 'sidebar-position', 'side="left"' ) !== 'disabled' ) : ?>
		<button class="hamburger hamburger--spin site-col-auto site-sidebar-opener site-sidebar-toggle" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	<?php endif; ?>

	<?php linna_logo(); ?>

	<?php if ( class_exists( 'woocommerce' ) ) : ?>
		<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'linna' ); ?>" class="site-col-auto site-header-icon-container site-woocommerce-header-count">
			<?php echo wp_kses( linna_get_icon_svg( 'shopping-cart', 20, 20 ), linna_get_kses_extended_ruleset() ); ?>
			<span><?php echo esc_html( WC()->cart->get_cart_contents_count() ); ?></span>
		</a>
	<?php endif; ?>

<!--	<a href="#" class="site-col-auto site-header-icon-container">--><?php //echo wp_kses( linna_get_icon_svg( 'envelope', 18, 18 ), linna_get_kses_extended_ruleset() ); ?><!--</a>-->

	<?php
	$description = get_bloginfo( 'description', 'display' );
	if ( $description || is_customize_preview() ) :
		?>
		<!--<p class="site-description">
				<?php echo esc_html( $description ); ?>
			</p>-->
	<?php endif; ?>

	<?php if ( has_nav_menu( 'social' ) ) : ?>
		<!-- <nav class="social-navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'linna' ); ?>">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'social',
					'menu_class'     => 'social-links-menu',
					'link_before'    => '<span class="screen-reader-text">',
					'link_after'     => '</span>' . linna_get_icon_svg( 'link' ),
					'depth'          => 1,
				)
			);
			?>
		</nav>.social-navigation -->
	<?php endif; ?>
</div>
