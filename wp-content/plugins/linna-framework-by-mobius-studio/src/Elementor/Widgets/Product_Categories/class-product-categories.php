<?php
/**
 * Product Categories Widget.
 *
 * @package Linna Framework
 * @category Mobius Studio Elementor
 * @since 1.0.0
 */

namespace Linna_Framework\Elementor\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Plugin;
use Elementor\Core\Schemes;
use Elementor\Widget_Base;
use Linna_Framework\Elementor\Helpers;

/**
 * Elementor Product Categories widget.
 *
 * A custom widget, supports blog posts with carousel mode.
 *
 * @since 1.0.0
 */
class Product_Categories extends Widget_Base implements Widget {

	/**
	 * Get widget name.
	 *
	 * Retrieve Product Categories widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_name() {
		return 'product_categories';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Product Categories widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_title() {
		return __( 'Product Categories', 'linna-framework-by-mobius-studio' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Product Categories widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_icon() {
		return 'eicon-product-categories';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Product Categories widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_categories() {
		return array( 'linna-framework' );
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @return array Widget keywords.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_keywords() {
		return array( 'product', 'category', 'slider', 'categories', 'woocommerce' );
	}

	/**
	 * Register Product Categories widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		/*TYPE
		off screen carousel
		columns

		STYLING
		box background
		box border
		box border-radius
		box shadow

		icon width %
		icon color

		text color
		text font*/



	}

	/**
	 * Render Product Categories widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		// $settings = $this->get_settings_for_display();

		// See https://developer.wordpress.org/reference/classes/wp_term_query/__construct/ .
		$settings['taxonomy']     = 'product_cat';
		$settings['orderby']      = 'name';
		$settings['show_count']   = 0;
		$settings['pad_counts']   = 0;
		$settings['hierarchical'] = 1;
		$settings['title']        = '';
		$settings['empty']        = 0;

		$all_categories = get_categories( $settings );

		if ( empty( $all_categories ) ) {
			return;
		}

		$items = array();

		$svg_chevron = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" ><path fill="currentColor" d="M24.707 38.101L4.908 57.899c-4.686 4.686-4.686 12.284 0 16.971L185.607 256 4.908 437.13c-4.686 4.686-4.686 12.284 0 16.971L24.707 473.9c4.686 4.686 12.284 4.686 16.971 0l209.414-209.414c4.686-4.686 4.686-12.284 0-16.971L41.678 38.101c-4.687-4.687-12.285-4.687-16.971 0z"></svg>';

		foreach ( $all_categories as $category ) {

			$items[] = sprintf(
				'<a href="%s"><i>%s</i><span>%s %s</span></a>',
				esc_url( get_term_link( $category->slug, 'product_cat' ) ),
				wp_kses( get_term_meta( $category->term_id, 'linna_wc_category_icon', true ), linna_get_kses_extended_ruleset() ),
				esc_html( $category->name ),
				wp_kses( $svg_chevron, linna_get_kses_extended_ruleset() ),
			);

		}

		require_once __DIR__ . '/view.php';
	}

}
