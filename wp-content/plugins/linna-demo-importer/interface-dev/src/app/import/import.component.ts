import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {HostCheckService} from "../_services/host-check.service";
import {DemoService} from "../_services/demo.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatStepper} from "@angular/material/stepper";
import {Demo} from "../_classes/demo";
import {TranslationsService} from "../_services/translations.service";

@Component({
	selector: 'app-import',
	templateUrl: './import.component.html',
	styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
	isLinear = false;
	hostCheck: FormGroup;
	demoSelection: FormGroup;
	importConfirmationForm: FormGroup;
	importAction: FormGroup;
	importing: boolean = false;
	showAssetList: boolean = false;
	completed: boolean  = false;
	translations:any[] = [];
	@ViewChild('stepper') private myStepper: MatStepper;

	constructor(
		private _formBuilder: FormBuilder,
		private http: HttpClient,
		private hostCheckService: HostCheckService,
		public demoService: DemoService,
		public dialog: MatDialog,
		private window: Window,
		public translationService: TranslationsService,
	) {
	}

	get formHostCheck() {
		return this.hostCheck as FormGroup;
	}

	get formHostCheckControls() {
		return this.formHostCheck.controls;
	}

	get formHostCheckLoading() {
		return this.formHostCheckControls.loading as FormGroup;
	}

	get formHostCheckLoadingControls() {
		return this.formHostCheckLoading.controls;
	}

	get formImportAction(){
		return this.importAction as FormGroup;
	}

	get formImportActionControls(){
		return this.formImportAction.controls;
	}

	get importActionMediaFormArray(){
		return this.formImportActionControls.media as FormArray;
	}

	get importActionMediaFormArrayControls():FormGroup[]{
		return this.importActionMediaFormArray.controls as FormGroup[];
	}

	ngOnInit() {

		this.translationService.translate.subscribe((translations) => {
			this.translations = translations
		});

		this.hostCheck = this._formBuilder.group({
			host_check_wp_filesystem_read: [{value: null, disabled: false}, Validators.required],
			host_check_wp_filesystem_create_dir: [{value: null, disabled: false}, Validators.required],
			host_check_wp_filesystem_copy: [{value: null, disabled: false}, Validators.required],
			host_check_db_rw: [{value: null, disabled: false}, Validators.required],
			host_check_max_execution_time: [{value: null, disabled: false}, Validators.required],
			host_check_memory_limit: [{value: null, disabled: false}, Validators.required],
			host_check_post_max_size: [{value: null, disabled: false}, Validators.required],
			host_check_upload_max_filesize: [{value: null, disabled: false}, Validators.required],
			host_check_wp_remote_get: [{value: null, disabled: false}, Validators.required],

			loading: this._formBuilder.group({
				host_check_wp_filesystem_read: [{value: null, disabled: false}, Validators.required],
				host_check_wp_filesystem_create_dir: [{value: null, disabled: false}, Validators.required],
				host_check_wp_filesystem_copy: [{value: null, disabled: false}, Validators.required],
				host_check_db_rw: [{value: null, disabled: false}, Validators.required],
				host_check_max_execution_time: [{value: null, disabled: false}, Validators.required],
				host_check_memory_limit: [{value: null, disabled: false}, Validators.required],
				host_check_post_max_size: [{value: null, disabled: false}, Validators.required],
				host_check_upload_max_filesize: [{value: null, disabled: false}, Validators.required],
				host_check_wp_remote_get: [{value: null, disabled: false}, Validators.required],
			}),

			success: [{value: null, disabled: false}, Validators.required],
			errors: [{value: null, disabled: false}],
		});

		this.hostCheckService.checkFromFormFields(this.hostCheck);

		this.demoSelection = this._formBuilder.group({
			index_of_demo_to_install: [null, Validators.required]
		});

		this.importConfirmationForm = this._formBuilder.group({
			confirm: [null, Validators.required]
		});

		this.importAction = this._formBuilder.group({
			media_status: [null, Validators.requiredTrue],
			media: new FormArray([]),
			sql_imp: [null],
			sql_zip: [null],
			options: [null],
			sql_status: [null, Validators.requiredTrue],
		});
	}

	translate(key){
		return this.translations[key] || '';
	}

	async loadImportActionFormWithChosenDemoData(demo:Demo){
		this.window.addEventListener('beforeunload', (e) => {
			if ( this.completed ) return true;
			e.returnValue = null;
			return null;
		});

		// console.log(demo);
		// console.log(mediaUrl)
		Object.keys(demo.media).forEach(mediaUrl => {
			// console.log(demo.media[mediaUrl]);
			this.importActionMediaFormArray.push(
				this._formBuilder.group({
					url: [demo.media[mediaUrl], Validators.required],
					complete: [null, Validators.requiredTrue],
				}),
			)
		});

		if ( demo.sql ){
			if( demo.sql.zip ){
				this.formImportActionControls.sql_zip.setValue(demo.sql.zip);
			}

			if( demo.sql.imp ){
				this.formImportActionControls.sql_imp.setValue(demo.sql.imp);
			}

			if( demo.sql.options ){
				this.formImportActionControls.options.setValue(demo.sql.options);
			}
		}
		// return;
		await this.demoService.processImport(this.formImportAction);
		// console.log(this.importActionMediaFormArray.controls);

		this.importAction.valueChanges.subscribe((form) => {
			// console.log('this.importAction', this.importAction.valid);
			if (this.importAction.valid){
				this.myStepper.next();
				this.completed = true;
			}
		});
	}

	validMediaAmount(): number{
		return this.importActionMediaFormArrayControls.filter(control => control.valid).length;
	}

	selectDemo(i, demo){
		this.demoSelection.controls.index_of_demo_to_install.setValue(i);
		this.openDialog(demo);
	}

	openDialog(demo): void {
		const dialogRef = this.dialog.open(ImportConfirmationDialog, {
			width: '350px',
			data: this.importConfirmationForm
		});

		dialogRef.afterClosed().subscribe((result:FormGroup) => {
			// console.log('The dialog was closed');
			if ( result && result.valid ){
				this.myStepper.next();
				if ( this.myStepper.selectedIndex === 2 ){
					this.loadImportActionFormWithChosenDemoData(demo);
					this.importing = true;
				}
			}
			// this.animal = result;
		});
	}
}

@Component({
	selector: 'import-confirmation-dialog',
	templateUrl: 'import-confirmation-dialog.html',
})
export class ImportConfirmationDialog {
	translations:any[] = [];

	constructor(
		public dialogRef: MatDialogRef<ImportConfirmationDialog>,
		@Inject(MAT_DIALOG_DATA) public importConfirmationForm: FormGroup,
		public translationService: TranslationsService,
		) {}

	onNoClick(): void {
		this.importConfirmationForm.controls.confirm.setValue(null);
		this.dialogRef.close();
	}
	ngOnInit() {

		this.translationService.translate.subscribe((translations) => {
			this.translations = translations
		});
	}

	translate(key){
		return this.translations[key] || '';
	}

}
